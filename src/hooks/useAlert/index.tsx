import { AlertOptions } from '@ionic/core';
import { IonAlert } from '@ionic/react';
import React, { useState } from 'react';

export default (props: AlertOptions) => {
  const [isOpen, setIsOpen] = useState(false);

  return {
    // eslint-disable-next-line react/jsx-props-no-spreading
    element: <IonAlert isOpen={isOpen} onDidDismiss={() => setIsOpen(false)} {...props} />,
    open: () => setIsOpen(true),
  };
};
