import { Plugins } from '@capacitor/core';
import { User } from '@helpinghands/types';
import Axios from 'axios';
import { User as FbUser } from 'firebase/app';
import { useEffect, useState } from 'react';
import { UserWithOpenTasks } from '../../components/Authentication/Provider';
import apiRequest from '../../utils/api-request';

const { Storage } = Plugins;

/**
 * Updates UserData to local storage
 * @param data
 */
export const updateLocalUserData = (data: UserWithOpenTasks) =>
  Storage.set({
    key: 'user-data',
    value: JSON.stringify(data),
  }).then((): Promise<UserWithOpenTasks> => Promise.resolve(data));

/**
 * Get local cached UserData
 */
export const getUserData = (): Promise<UserWithOpenTasks> =>
  Storage.get({ key: 'user-data' }).then(({ value }) =>
    value ? Promise.resolve(JSON.parse(value) as UserWithOpenTasks) : Promise.reject(),
  );

export const clearUserData = (): Promise<void> => Storage.remove({ key: 'user-data' });

/**
 * Updates UserData to Backend and save local
 * @param data
 */
export const updateUserData = (data: Partial<User>) =>
  apiRequest
    .put<'/user'>('/user', data)
    .then(() => getUserData())
    .then((user) =>
      updateLocalUserData({
        ...user,
        ...data,
      }),
    );

export const createUserData = (payload: User) =>
  apiRequest
    .post<'/user'>('/user', payload)
    .then(({ data }) => updateLocalUserData({ ...data, openTasks: 0 }));

export default (fbUser: FbUser | null) => {
  const [user, setUser] = useState<UserWithOpenTasks>();

  useEffect(() => {
    if (fbUser && !user) {
      (async () => {
        try {
          Axios.defaults.headers.common.Authorization = await fbUser.getIdToken();

          // fist use cache
          await getUserData()
            .then(setUser)
            .catch(() => Promise.resolve());

          await apiRequest
            .get<'/user'>('/user')
            .then(({ data }) => updateLocalUserData(data))
            .then(setUser)
            .catch(() => {
              clearUserData();
              setUser(undefined);
            });
        } catch (e) {
          // ignore
        }
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fbUser]);

  return {
    user,
    setUser,
  };
};
