import { ShoppingListWithUser } from '@helpinghands/types';
import { useEffect } from 'react';
import { useLocation } from 'react-router';

export type HistoryState = {
  deleteList?: string;
  updateList?: ShoppingListWithUser;
  addLists?: ShoppingListWithUser[];
};

/**
 * Use HistoryState to update data without new Fetch
 */
export default ({
  data,
  setData,
}: {
  data?: ShoppingListWithUser[];
  setData: (value: ShoppingListWithUser[]) => void;
}) => {
  const { state } = useLocation<HistoryState>();

  useEffect(() => {
    if (data) {
      const { deleteList, updateList, addLists } = state || {};
      if (deleteList) {
        setData(data.filter(({ id }) => id !== deleteList));
      }
      if (updateList) {
        setData(data.map((list) => (list.id === updateList.id ? updateList : list)));
      }
      if (addLists) {
        setData([...addLists, ...data]);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state]);
};
