import firebase, { User } from 'firebase/app';
import { useEffect, useState } from 'react';
import authInstance, { auth } from '../../utils/firebase/authentication';

export default (authToken?: string) => {
  const [user, setUser] = useState<User | null>(authInstance.currentUser);
  // Firebase
  useEffect(() => {
    if (authToken && !user) {
      auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(authToken));
    }
    return authInstance.onAuthStateChanged(setUser);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authToken]);

  return user;
};
