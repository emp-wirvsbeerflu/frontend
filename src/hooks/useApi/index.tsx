import ShoppingListApi from '@helpinghands/types';
import { useCallback, useEffect, useState } from 'react';
import { useNotification } from '../../components/Global/Notification/Toast';
import { APIRoutes } from '../../utils';
import apiRequest from '../../utils/api-request';

/**
 * Hook for GET Requests from API
 * Usage: `const data = useApiData<"/shoppinglist/:id">(`/shoppinglist/${id}`, key)`
 * use key to force new fetch
 */
const useApiData = <T extends APIRoutes['GET']>(url: T | string | undefined, key: unknown = '') => {
  const { setError } = useNotification();
  const [data, setData] = useState<ShoppingListApi[T]['GET']['return']>();

  const getData = useCallback(() => {
    if (url) {
      return apiRequest
        .get<T>(url)
        .then((res) => setData(res.data))
        .catch(() => setError());
    }
    return Promise.resolve();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [url]);

  useEffect(() => {
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [url, key]);

  return { data, setData, getData };
};

export default useApiData;
