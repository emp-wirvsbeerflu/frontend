import { Plugins } from '@capacitor/core';
import '@codetrix-studio/capacitor-google-auth';
import { isPlatform } from '@ionic/core';
import firebase from 'firebase/app';
import { useCallback, useState } from 'react';
import { useNotification } from '../../components/Global/Notification/Toast';
import authInstance from '../../utils/firebase/authentication';

export default () => {
  const { setError } = useNotification();
  const [authToken, setAuthToken] = useState<string>();

  const signIn = useCallback(async () => {
    try {
      if (isPlatform('capacitor')) {
        const {
          authentication: { idToken },
        } = await Plugins.GoogleAuth.signIn();
        setAuthToken(idToken);
      } else {
        authInstance.signInWithPopup(new firebase.auth.GoogleAuthProvider());
      }
    } catch (e) {
      setError();
    }
  }, [setError]);

  return {
    authToken,
    signIn,
  };
};
