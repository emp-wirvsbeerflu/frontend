import { Plugins } from '@capacitor/core';
import axios from 'axios';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import config from './config';
import * as serviceWorker from './serviceWorker';

const { SplashScreen } = Plugins;

axios.defaults.baseURL = config.baseUrl;

try {
  SplashScreen.hide();
} catch (e) {
  // ignore
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
if (config.useServiceWorker) {
  serviceWorker.register();
} else {
  serviceWorker.unregister();
}
