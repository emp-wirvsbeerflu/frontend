import { UserType } from '@helpinghands/types';
import { IonApp, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import '@ionic/react/css/core.css';
import '@ionic/react/css/display.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/padding.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';
import React, { FC } from 'react';
import { IntlProvider } from 'react-intl';
import AuthGuard from './components/Authentication/Guard';
import { useUser } from './components/Authentication/Provider';
import RegisterPushNotification from './components/Global/Notification/Push';
import NotificationProvider from './components/Global/Notification/Toast';
import SideMenu from './components/Global/SideMenu';
import HeroRouter from './components/Hero';
import HomieRouter from './components/Homie';
import './theme/index.scss';
import './theme/variables.scss';
// import './theme/dark.scss';

const Router = () => {
  const user = useUser();

  if (user.type === UserType.HERO) {
    return <HeroRouter />;
  }
  return <HomieRouter />;
};

const App: FC = () => (
  <IntlProvider locale="de-DE">
    <IonApp>
      <NotificationProvider>
        <IonReactRouter>
          <AuthGuard>
            <RegisterPushNotification />
            <IonSplitPane contentId="main">
              <SideMenu />
              <Router />
            </IonSplitPane>
          </AuthGuard>
        </IonReactRouter>
      </NotificationProvider>
    </IonApp>
  </IntlProvider>
);

export default App;
