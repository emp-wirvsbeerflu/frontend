import { IonButton, IonIcon, IonPopover } from '@ionic/react';
import { ellipsisVertical } from 'ionicons/icons';
import React, { FC, useState } from 'react';
import withErrorBoundary from '../ErrorBoundary';

const Popover: FC = ({ children }) => {
  const [event, setEvent] = useState<MouseEvent>();

  return (
    <>
      {event && (
        <IonPopover
          event={event}
          showBackdrop={false}
          isOpen={!!event}
          onDidDismiss={() => setEvent(undefined)}
        >
          {children}
        </IonPopover>
      )}
      <IonButton onClick={({ nativeEvent }) => setEvent(nativeEvent)}>
        <IonIcon slot="icon-only" icon={ellipsisVertical} />
      </IonButton>
    </>
  );
};

export default withErrorBoundary(Popover);
