import { UserType } from '@helpinghands/types';
import {
  IonAvatar,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonItemGroup,
  IonLabel,
  IonList,
  IonMenu,
  IonMenuButton,
  IonMenuToggle,
  IonText,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import { closeOutline } from 'ionicons/icons';
import React, { FC } from 'react';
import { FormattedPlural } from 'react-intl';
import { useLocation } from 'react-router-dom';
import { useUser } from '../../Authentication/Provider';
import config from '../../../config';

const navContent: {
  [U in UserType]: {
    openTasks: {
      one: string;
      other: string;
    };
    items: {
      url: string;
      icon: string;
      title: string;
    }[];
  };
} = {
  HERO: {
    openTasks: {
      one: 'laufender Auftrag',
      other: 'laufende Aufträge',
    },
    items: [
      {
        title: 'Aufträge finden',
        icon: 'search',
        url: '/homies',
      },
      {
        title: 'Meine Aufträge',
        icon: 'shopping-cart',
        url: '/',
      },
      {
        title: 'Profileinstellungen',
        icon: 'settings',
        url: '/settings',
      },
      {
        title: 'Support',
        icon: 'support',
        url: '/support',
      },
    ],
  },
  HOMIE: {
    openTasks: {
      one: 'offener Einkaufszettel',
      other: 'offene Einkaufszettel',
    },
    items: [
      {
        title: 'Meine Einkaufszettel',
        icon: 'shopping-cart',
        url: '/',
      },
      {
        title: 'Profileinstellungen',
        icon: 'settings',
        url: '/settings',
      },
      {
        title: 'Support',
        icon: 'support',
        url: '/support',
      },
    ],
  },
};

const SideMenu: FC = () => {
  const { pathname } = useLocation();

  const user = useUser();

  const { items, openTasks } = navContent[user.type];

  return (
    <IonMenu contentId="main" type="overlay">
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton>
              <IonIcon icon={closeOutline} />
            </IonMenuButton>
          </IonButtons>
          <IonTitle>{config.isProduction ? 'Menü' : 'STAGING'}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        {user && (
          <IonItemGroup
            style={{
              marginTop: '32px',
            }}
          >
            <IonItem lines="none">
              <IonAvatar
                style={{
                  width: '64px',
                  height: '64px',
                }}
              >
                {user.photoURL && <img alt="profile" src={user.photoURL} />}
              </IonAvatar>
            </IonItem>
            <IonItem lines="full">
              <IonLabel>
                <h1>{user.displayName}</h1>
                <IonText color="medium">
                  {`${user.openTasks || 0} `}
                  <FormattedPlural
                    value={user.openTasks || 0}
                    one={openTasks.one}
                    other={openTasks.other}
                  />
                </IonText>
              </IonLabel>
            </IonItem>
          </IonItemGroup>
        )}

        <IonList className="ion-margin-top">
          {items.map(({ icon, url, title }) => (
            <IonMenuToggle key={url} autoHide={false}>
              <IonItem
                style={
                  pathname !== url
                    ? { color: 'var(--ion-color-primary)' }
                    : {
                        fontWeight: 'bold',
                      }
                }
                routerLink={url}
                routerDirection="none"
                lines="none"
              >
                <IonIcon src={`/assets/icons/${icon}.svg`} slot="start" />
                <IonLabel>{title}</IonLabel>
              </IonItem>
            </IonMenuToggle>
          ))}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default SideMenu;
