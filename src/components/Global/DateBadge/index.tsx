import { IonBadge, IonButton, IonIcon } from '@ionic/react';
import { close } from 'ionicons/icons';
import React, { FC } from 'react';

const DateBadge: FC<{
  value: number;
  /** If onClear it is a clearable button else a badge */
  onClear?: () => void;
}> = ({ value, onClear }) =>
  onClear ? (
    <IonButton
      color="primary"
      shape="round"
      size="small"
      onClick={onClear}
      style={{
        borderRadius: '12px',
        display: 'inline-block',
        padding: '0',
        height: '22px',
        fontSize: '12px',
        textTransform: 'none',
        margin: '0',
      }}
    >
      bis {new Date(value).toLocaleDateString()}
      <IonIcon icon={close} slot="end" />
    </IonButton>
  ) : (
    <IonBadge>bis {new Date(value).toLocaleDateString()}</IonBadge>
  );

export default DateBadge;
