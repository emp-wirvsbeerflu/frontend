import React, { Component, ErrorInfo, FC } from 'react';

function withErrorBoundary<ComponentProps>(
  WrappedComponent: FC<ComponentProps>,
  ErrorComponent: FC = () => null,
  errorCallback: (error: Error, errorInfo: ErrorInfo, props: ComponentProps) => void = () => {},
) {
  return class WithErrorBoundary extends Component<ComponentProps> {
    // eslint-disable-next-line react/static-property-placement
    static displayName = `withErrorBoundary(${
      WrappedComponent.displayName || WrappedComponent.name
    })`;

    // eslint-disable-next-line react/state-in-constructor
    readonly state = false;

    componentDidCatch(error: Error, info: ErrorInfo) {
      this.setState(true);

      errorCallback(error, info, this.props);
    }

    render() {
      const hasError = this.state;

      if (hasError) {
        // eslint-disable-next-line react/jsx-props-no-spreading
        return <ErrorComponent {...this.props} />;
      }

      // eslint-disable-next-line react/jsx-props-no-spreading
      return <WrappedComponent {...this.props} />;
    }
  };
}

export default withErrorBoundary;
