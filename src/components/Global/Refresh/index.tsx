import { IonRefresher, IonRefresherContent } from '@ionic/react';
import React, { FC } from 'react';

const Refresher: FC<{ getData: () => Promise<void> }> = ({ getData }) => (
  <div>
    <IonRefresher
      slot="fixed"
      onIonRefresh={(event) => getData().then(() => event.detail.complete())}
    >
      <IonRefresherContent />
    </IonRefresher>
  </div>
);

export default Refresher;
