import {
  IonButton,
  IonButtons,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonIcon,
  IonToolbar,
  IonItemDivider,
} from '@ionic/react';
import { chevronDown, chevronUp } from 'ionicons/icons';
import React, { FC, useState } from 'react';

const CollapseCard: FC<{
  title: string | JSX.Element;
  subtitle?: string | JSX.Element;
  headerStartSlot?: JSX.Element;
  headerBottomtSlot?: JSX.Element;
  bottomSlot?: JSX.Element;
  style?: object;
}> = ({
  title,
  subtitle,
  children,
  headerStartSlot,
  bottomSlot,
  style,
  headerBottomtSlot = null,
}) => {
  const [collapse, setCollapse] = useState(false);

  return (
    <IonCard className="ion-margin-bottom" style={style}>
      <IonCardHeader>
        <IonToolbar color="transparent">
          {headerStartSlot && (
            <IonButtons slot="start" className="ion-padding-end">
              {headerStartSlot}
            </IonButtons>
          )}
          <IonCardTitle>{title}</IonCardTitle>
          {subtitle && <IonCardSubtitle>{subtitle}</IonCardSubtitle>}
          <IonButtons slot="end">
            <IonButton size="small" slot="end" onClick={() => setCollapse(!collapse)}>
              <IonIcon slot="icon-only" icon={collapse ? chevronUp : chevronDown} />
            </IonButton>
          </IonButtons>
        </IonToolbar>
        {headerBottomtSlot}
      </IonCardHeader>
      {collapse && (
        <>
          <IonItemDivider style={{ minHeight: '0' }} />
          {children}
        </>
      )}
      {bottomSlot && bottomSlot}
    </IonCard>
  );
};
export default CollapseCard;
