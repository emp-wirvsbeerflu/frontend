import React, { FC } from 'react';
import { IonSpinner } from '@ionic/react';

const Loading: FC<{ isLoading: boolean }> = ({ isLoading, children }) =>
  isLoading ? (
    <div
      style={{
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <IonSpinner className="ion-align-center" />
    </div>
  ) : (
    <>{children}</>
  );

export default Loading;
