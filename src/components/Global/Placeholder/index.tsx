import React, { FC } from 'react';

const Placeholder: FC<{
  image: string;
}> = ({ children, image }) => (
  <div className="placeholder">
    <div
      style={{
        backgroundImage: `url(/assets/placeholder/${image}.svg)`,
      }}
    />
    <div>{children}</div>
  </div>
);

export default Placeholder;
