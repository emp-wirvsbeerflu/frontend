import { Plugins, PushNotificationToken } from '@capacitor/core';
import { isPlatform } from '@ionic/core';
import { FC, useEffect } from 'react';
import messaging from '../../../../utils/firebase/messaging';
import { useUser } from '../../../Authentication/Provider';

const { PushNotifications } = Plugins;

const registerPush = (update: (token: string) => void) => {
  // Register fcm for Native Apps
  if (isPlatform('capacitor')) {
    PushNotifications.requestPermission().then(() => {
      PushNotifications.addListener('registration', (token: PushNotificationToken) =>
        update(token.value),
      );
      PushNotifications.register();
    });
    return () => PushNotifications.removeAllListeners();
  }

  // Register fcm for PWA
  return messaging(update);
};

const RegisterPushNotification: FC = () => {
  const user = useUser();

  useEffect(
    () => {
      try {
        return registerPush((notificationToken) => {
          if (notificationToken && notificationToken !== user.notificationToken) {
            user.updateUser({
              notificationToken,
            });
          }
        });
      } catch (e) {
        return () => {};
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return null;
};

export default RegisterPushNotification;
