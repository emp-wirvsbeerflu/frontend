import { IonToast } from '@ionic/react';
import { checkmarkCircleOutline, closeOutline, alertCircleOutline } from 'ionicons/icons';
import React, { createContext, FC, useContext, useState } from 'react';

interface Notification {
  type: 'error' | 'success';
  message: string;
  key: string;
}

interface NotificationContext {
  setError: (value?: string) => void;
  setSuccess: (value: string) => void;
}

const NotificationContext = createContext<NotificationContext>({
  setError: () => {},
  setSuccess: () => {},
});

export const useNotification = () => useContext(NotificationContext);

const NotificationProvider: FC = ({ children }) => {
  const [notification, setNotification] = useState<Notification>();

  return (
    <NotificationContext.Provider
      value={{
        setSuccess: (message: string) =>
          setNotification({
            message,
            type: 'success',
            key: `${Math.random()}`,
          }),
        setError: (message = 'Ups, leider ist etwas schiefgelaufen..') =>
          setNotification({
            type: 'error',
            message,
            key: `${Math.random()}`,
          }),
      }}
    >
      {notification && (
        <IonToast
          isOpen
          cssClass={notification.type}
          key={notification.key}
          message={notification.message}
          position="top"
          buttons={[
            {
              side: 'start',
              icon: notification.type === 'success' ? checkmarkCircleOutline : alertCircleOutline,
            },
            {
              icon: closeOutline,
              handler: () => setNotification(undefined),
            },
          ]}
          duration={3000}
        />
      )}
      {children}
    </NotificationContext.Provider>
  );
};

export default NotificationProvider;
