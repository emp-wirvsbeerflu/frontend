import React, { FC } from 'react';
import { GoogleMap, withGoogleMap } from 'react-google-maps';
import { LatLon } from 'use-places-autocomplete';

const Map: FC<{
  location: LatLon;
}> = ({ location, children }) => (
  <GoogleMap
    defaultZoom={9}
    defaultCenter={location}
    defaultOptions={{
      fullscreenControl: false,
      mapTypeControl: false,
      streetViewControl: false,
    }}
  >
    {children}
  </GoogleMap>
);

export default withGoogleMap(Map);
