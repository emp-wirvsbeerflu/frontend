import { Address } from '@helpinghands/types';
import { IonCard, IonInput, IonItem, IonLabel, IonList } from '@ionic/react';
import React, { FC, useEffect, useMemo, useState } from 'react';
import usePlacesAutocomplete, {
  GeocodeResult,
  getGeocode,
  getLatLng,
  LatLon,
} from 'use-places-autocomplete';
import withErrorBoundary from '../../../ErrorBoundary';
import { useNotification } from '../../../Notification/Toast';

const getAddress = (
  // eslint-disable-next-line @typescript-eslint/camelcase
  { address_components }: GeocodeResult,
  key: 'street_number' | 'route' | 'locality' | 'postal_code',
) => {
  // eslint-disable-next-line @typescript-eslint/camelcase
  const [{ long_name = '' } = {}] = address_components.filter(({ types }) => types.includes(key));
  // eslint-disable-next-line @typescript-eslint/camelcase
  return long_name;
};

const AutoComplete: FC<{
  setLocation: (value?: LatLon) => void;
  setAddress: (value?: Address) => void;
  initialAddress?: string;
}> = ({ setLocation, setAddress, initialAddress }) => {
  const [showSuggestions, setShowSuggestions] = useState(true);
  const [inputValue, setInputValue] = useState(initialAddress);
  const [suggestion, setSuggestion] = useState<string>();
  const { setValue, suggestions } = usePlacesAutocomplete({
    requestOptions: {
      bounds: new google.maps.LatLngBounds(
        new google.maps.LatLng(45.874386, 6.436407),
        new google.maps.LatLng(54.97104, 14.522345),
      ),
    },
  });

  useEffect(() => {
    if (inputValue?.length && inputValue !== initialAddress) {
      setValue(inputValue);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inputValue]);

  const { setError } = useNotification();

  useMemo(async () => {
    if (suggestion) {
      try {
        const [res] = await getGeocode({ address: suggestion });
        setLocation(await getLatLng(res));
        setAddress({
          houseNumber: getAddress(res, 'street_number'),
          street: getAddress(res, 'route'),
          city: getAddress(res, 'locality'),
          postalCode: getAddress(res, 'postal_code'),
        });
      } catch (e) {
        setError();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setLocation, suggestion]);

  // show suggestions when they changed
  useEffect(() => {
    if (!suggestions.loading && suggestions.data.length && inputValue !== suggestion) {
      setShowSuggestions(true);
      setLocation(undefined);
      setAddress(undefined);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setAddress, setLocation, suggestions.loading]);

  return (
    <>
      <div className="addressForm">
        <IonItem>
          <IonLabel position="floating">Ort / Postleizahl</IonLabel>
          <IonInput
            value={inputValue || ''}
            onIonChange={({ detail: { value } }) => setInputValue(value || '')}
          />
        </IonItem>
      </div>
      <div>
        {!!suggestions?.data?.length && showSuggestions && (
          <IonCard className="ion-no-margin">
            <IonList>
              {suggestions.data.map(({ id, description }, i) => (
                <IonItem
                  key={id}
                  onClick={() => {
                    setSuggestion(description);
                    setInputValue(description);
                    setShowSuggestions(false);
                  }}
                  button
                  lines={i === suggestions.data.length - 1 ? 'none' : 'full'}
                >
                  {description}
                </IonItem>
              ))}
            </IonList>
          </IonCard>
        )}
      </div>
    </>
  );
};

export default withErrorBoundary(AutoComplete);
