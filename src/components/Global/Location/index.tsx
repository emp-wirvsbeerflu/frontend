import { Address } from '@helpinghands/types';
import { IonLabel, IonRange, IonInput } from '@ionic/react';
import React, { FC } from 'react';
import { Circle, Marker } from 'react-google-maps';
import { LatLon } from 'use-places-autocomplete';
import AutoComplete from './GoogleMaps/AutoComplete';
import Map from './GoogleMaps/Map';

const Location: FC<{
  button: JSX.Element;
  radius: number;
  location?: LatLon;
  setRadius: (val: number) => void;
  setAddress: (val: Address | undefined) => void;
  setLocation: (val: LatLon | undefined) => void;
  initialAddress?: string;
}> = ({ radius, setRadius, location, setAddress, setLocation, button, initialAddress }) => (
  <>
    <AutoComplete
      setLocation={setLocation}
      setAddress={setAddress}
      initialAddress={initialAddress}
    />
    {location && (
      <>
        <Map
          containerElement={<div style={{ height: '400px', padding: '8px' }} />}
          key={`${location.lat}-${location.lng}`}
          mapElement={<div style={{ height: '100%' }} />}
          location={location}
        >
          <Circle
            radius={radius * 1000}
            center={location}
            options={{
              fillOpacity: 0.2,
              strokeWeight: 1,
            }}
          />
          <Marker position={location} icon="/assets/icons/marker.svg" />
        </Map>
        <IonRange
          min={1}
          max={30}
          value={radius}
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          onIonChange={({ target: { value = 0 } }: any) => setRadius(value)}
          pin
        >
          <IonLabel slot="start">1km</IonLabel>
          <IonLabel slot="end">30km</IonLabel>
        </IonRange>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <IonInput
            className="ion-no-padding ion-margin"
            style={{
              flex: '0 0 auto',
              width: 'auto',
              border: '1px solid currentColor',
              borderRadius: '4px',
              textAlign: 'center',
            }}
            type="number"
            min="1"
            max="100"
            value={radius}
            onIonChange={({ detail: { value } }) => {
              const newRadius = parseInt(value || '', 10);
              if (newRadius > 0) {
                setRadius(newRadius);
              }
            }}
          />
          km
        </div>
        {button}
      </>
    )}
  </>
);

export default Location;
