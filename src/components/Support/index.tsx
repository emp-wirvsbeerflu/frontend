import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import { mail } from 'ionicons/icons';
import React from 'react';

export default () => (
  <IonPage>
    <IonHeader>
      <IonToolbar color="primary">
        <IonButtons slot="start">
          <IonMenuButton />
        </IonButtons>
        <IonTitle>Support</IonTitle>
      </IonToolbar>
    </IonHeader>
    <IonContent className="ion-padding">
      <div
        style={{
          textAlign: 'center',
        }}
      >
        <div className="ion-margin-bottom">
          <img
            src="/assets/logo.svg"
            style={{
              width: '96px',
            }}
            alt="Helping Hands"
          />
          <h2>Eine Hand hilft der anderen</h2>
          <p>
            Egal ob Probleme, Wünsche, Anregungen, Beschwerden oder was dir noch so auf dem Herzen
            liegt. Wir sind für dich da.
          </p>
          <IonButton className="ion-margin-bottom" href="mailto:support@helping-hands.app">
            Via E-Mail kontaktieren
            <IonIcon slot="end" icon={mail} />
          </IonButton>
        </div>
        <hr />
        <div className="section">
          <h2>Wir legen Wert auf deine Sicherheit</h2>
          <div className="ion-margin">
            {' '}
            Unsere aktuelle Datenschutzerklärung und die Nutzungsbedingungen der Plattform findest
            du auf unserer Website über die nachfolgenden Links.
          </div>
          <div className="ion-margin">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://helping-hands.app/datenschutz/"
            >
              Datenschutz
            </a>{' '}
            |{' '}
            <a target="_blank" rel="noopener noreferrer" href="https://helping-hands.app/agb/">
              Nutzungsbedingungen
            </a>
          </div>
        </div>
      </div>
    </IonContent>
  </IonPage>
);
