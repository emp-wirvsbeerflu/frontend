import {
  Document,
  ShoppingList,
  ShoppingListState,
  ShoppingListWithUser,
} from '@helpinghands/types';
import { IonAvatar, IonIcon, IonItem, IonLabel, IonList, IonText, IonToggle } from '@ionic/react';
import { checkmarkCircleOutline } from 'ionicons/icons';
import React, { FC } from 'react';
import { listTypesNames } from '../../../../utils';
import apiRequest from '../../../../utils/api-request';
import { useUser } from '../../../Authentication/Provider';
import CollapseCard from '../../../Global/CollapsCard';
import DateBadge from '../../../Global/DateBadge';
import { useNotification } from '../../../Global/Notification/Toast';

type CardConfig = {
  headerBottomSlot?: JSX.Element;
  bottomSlot?: JSX.Element;
};

const Card: FC<{
  shoppingList: ShoppingListWithUser | Document<ShoppingList>;
  onUpdateState: (state: ShoppingListState) => void;
}> = ({
  shoppingList,
  shoppingList: { type, state, items, id, price, dueDate, comment },
  onUpdateState,
}) => {
  const { setError, setSuccess } = useNotification();
  const user = useUser();

  // CARD - STATE CONFIGURATIONS

  const TAKEN: CardConfig = {
    headerBottomSlot: (
      <div className="display-flex ion-justify-content-start ion-margin-bottom">
        {(shoppingList as ShoppingListWithUser).photoURL && (
          <IonAvatar
            style={{
              width: '32px',
              height: '32px',
            }}
            className="ion-margin-end"
          >
            <img src={(shoppingList as ShoppingListWithUser).photoURL} alt="avatar" />
          </IonAvatar>
        )}
        <IonLabel>
          {(shoppingList as ShoppingListWithUser).displayName} kauft für dich ein.
        </IonLabel>
      </div>
    ),
  };

  const DELIVERED: CardConfig = {
    bottomSlot: (
      <IonItem
        lines="none"
        button
        onClick={async () => {
          try {
            await apiRequest.post<'/shoppinglist/:id/close'>(`/shoppinglist/${id}/close`, {});
            onUpdateState(ShoppingListState.CLOSED);
            user.removeOpenTasks();
          } catch (e) {
            setError();
          }
        }}
      >
        <IonIcon slot="start" icon={checkmarkCircleOutline} />
        <IonLabel>Einkauf abschließen</IonLabel>
      </IonItem>
    ),
  };

  const IN_CREATION: CardConfig = {
    headerBottomSlot: (
      <div className="display-flex">
        <IonLabel>Einkaufszettel veröffentlichen</IonLabel>
        <IonToggle
          checked={state === ShoppingListState.PUBLISHED}
          color="primary"
          onIonChange={async ({ detail: { checked } }) => {
            try {
              if (checked) {
                await apiRequest.post<'/shoppinglist/:id/publish'>(
                  `/shoppinglist/${id}/publish`,
                  {},
                );
              } else {
                await apiRequest.post<'/shoppinglist/:id/unpublish'>(
                  `/shoppinglist/${id}/unpublish`,
                  {},
                );
              }
              onUpdateState(checked ? ShoppingListState.PUBLISHED : ShoppingListState.IN_CREATION);
              setSuccess(
                checked
                  ? 'Dein Einkaufszettel wurde veröffentlicht.'
                  : 'Dein Einkaufszettel ist jetzt nicht mehr öffentlich',
              );
            } catch (e) {
              setError();
            }
          }}
        />
      </div>
    ),
    bottomSlot: (
      <IonItem lines="none" routerLink={`/list/${id}`} button>
        <IonIcon slot="start" src="/assets/icons/pencil.svg" />
        <IonLabel>Einkaufszettel bearbeiten</IonLabel>
      </IonItem>
    ),
  };

  const PUBLISHED = IN_CREATION;

  const config = {
    TAKEN,
    DELIVERED,
    IN_CREATION,
    PUBLISHED,
    CLOSED: undefined,
    // show list as taken
    ON_DELIVERY: TAKEN,
  }[state];

  if (!config) {
    return null;
  }

  return (
    <CollapseCard
      title={
        <>
          {listTypesNames[type]} {price && <> - {price.toFixed(2)}€</>}
        </>
      }
      subtitle={
        <>
          {items.length} Artikel {dueDate && <DateBadge value={dueDate} />}
        </>
      }
      style={
        state === ShoppingListState.PUBLISHED
          ? {
              borderLeft: '5px solid var(--ion-color-primary)',
            }
          : undefined
      }
      headerBottomtSlot={
        <>
          {config.headerBottomSlot}
          {comment && (
            <IonText>
              Notiz:
              <br />
              {comment}
            </IonText>
          )}
        </>
      }
      bottomSlot={config.bottomSlot}
    >
      <IonList lines="none">
        {items
          .filter(({ done }) => state !== ShoppingListState.DELIVERED || done)
          .map(({ name, count }) => (
            <IonItem key={`${name}-${count}`}>
              <IonText slot="start">{count}x</IonText>
              <IonLabel>{name}</IonLabel>
            </IonItem>
          ))}
      </IonList>
    </CollapseCard>
  );
};

export default Card;
