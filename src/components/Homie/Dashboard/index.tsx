import { ShoppingListState, ShoppingListWithUser } from '@helpinghands/types';
import {
  IonAvatar,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonMenuButton,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
  IonButton,
} from '@ionic/react';
import React, { Fragment } from 'react';
import useApiData from '../../../hooks/useApi';
import useHistoryState from '../../../hooks/useHistoryState';
import { useUser } from '../../Authentication/Provider';
import Loading from '../../Global/Loading';
import Refresher from '../../Global/Refresh';
import Card from './Card';
import Placeholder from '../../Global/Placeholder';

export default () => {
  const user = useUser();
  const { photoURL, displayName } = user || {};

  const { data, setData, getData } = useApiData<'/homie/lists'>('/homie/lists');

  const editable = data?.filter(
    ({ state }) => state === ShoppingListState.IN_CREATION || state === ShoppingListState.PUBLISHED,
  );

  const delivered = data?.filter(({ state }) => state === ShoppingListState.DELIVERED);

  const taken = data?.filter(
    ({ state }) => state === ShoppingListState.TAKEN || state === ShoppingListState.ON_DELIVERY,
  );

  const lists = [
    {
      list: delivered,
      headline: 'Diese Einkaufszettel sollten noch bezahlt und bestätigt werden.',
    },
    {
      list: editable,
      headline: 'Hier findest du eine Übersicht deiner Einkaufszettel.',
    },
    {
      list: taken,
      headline: 'Diese Einkaufszettel werden gerade für dich eingekauft:',
    },
  ];

  // Use History State to update data without new fetch
  useHistoryState({
    data: data as ShoppingListWithUser[],
    setData,
  });

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Meine Einkaufszettel</IonTitle>
        </IonToolbar>
        <IonItem lines="none">
          {photoURL && (
            <IonAvatar slot="start">
              <img alt="avatar" src={photoURL} />
            </IonAvatar>
          )}
          <IonLabel>
            <h1>{displayName}</h1>
          </IonLabel>
        </IonItem>
      </IonHeader>
      <IonContent className="ion-padding">
        <Refresher getData={getData} />
        <Loading isLoading={!data}>
          {data?.length ? (
            <>
              {lists
                .filter(({ list }) => !!list?.length)
                .map(({ list = [], headline }, index) => (
                  <Fragment key={headline}>
                    <div
                      style={
                        index > 0
                          ? {
                              borderTop: '1px solid currentColor',
                            }
                          : undefined
                      }
                      className="ion-padding-vertical"
                    >
                      <IonText color="medium">{headline}</IonText>
                    </div>
                    {list.map((shoppingList) => (
                      <Card
                        shoppingList={shoppingList}
                        key={shoppingList.id}
                        onUpdateState={(state) =>
                          setData(
                            (data as ShoppingListWithUser[]).map((sList) =>
                              sList.id === shoppingList.id
                                ? {
                                    ...sList,
                                    state,
                                  }
                                : sList,
                            ),
                          )
                        }
                      />
                    ))}
                    {!data.find(({ state }) => state !== ShoppingListState.IN_CREATION) && (
                      <div>
                        Du hast noch keine Einkaufszettel veröffentlicht.
                        <br />
                        Deine Einkaufszettel werden erst für unsere Helfer sichtbar, wenn du das
                        möchtest.
                      </div>
                    )}
                  </Fragment>
                ))}

              <IonFab vertical="bottom" horizontal="end" slot="fixed">
                <IonFabButton routerLink="/list">
                  <IonIcon src="/assets/icons/add_cart.svg" />
                </IonFabButton>
              </IonFab>
            </>
          ) : (
            <Placeholder image="emptycart">
              <div className="form-hint">Du hast im Moment keine Einkaufszettel angelegt.</div>

              <IonButton className="ion-margin" routerLink="/list">
                Einkaufszettel anlegen <IonIcon slot="end" src="/assets/icons/add_cart.svg" />
              </IonButton>
            </Placeholder>
          )}
        </Loading>
      </IonContent>
    </IonPage>
  );
};
