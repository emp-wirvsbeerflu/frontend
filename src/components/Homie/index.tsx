import { IonRouterOutlet } from '@ionic/react';
import React from 'react';
import { Route } from 'react-router';
import Settings from '../Settings';
import Support from '../Support';
import CreateList from './CreateList';
import Dashboard from './Dashboard';

const HomieRouter = () => (
  <IonRouterOutlet id="main">
    <Route path="/" exact component={Dashboard} />
    <Route path={['/list/:id', '/list/']} component={CreateList} />
    <Route path="/settings" component={Settings} />
    <Route path="/support" component={Support} />
  </IonRouterOutlet>
);

export default HomieRouter;
