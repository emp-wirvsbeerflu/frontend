import { Address } from '@helpinghands/types';
import { IonInput, IonItem, IonLabel } from '@ionic/react';
import React, { FC } from 'react';
import { getGeocode, getLatLng } from 'use-places-autocomplete';

export const getLocation = async (address: Address) => {
  // get coordinates from address
  const [res] = await getGeocode({
    address: `${address?.postalCode} ${address?.city} ${address?.street}`,
  });

  return getLatLng(res);
};

const AddressForm: FC<{
  setAddress: (address: Partial<Address>) => void;
  address: Address;
  withPhone?: boolean;
  setPhone?: (phone: string) => void;
  phone?: string;
}> = ({ address = {}, setAddress, phone = '', setPhone = () => {}, withPhone = false }) => (
  <div>
    <div className="addressForm display-flex">
      <IonItem
        style={{
          flex: '1 1 100%',
        }}
      >
        <IonLabel position="floating">Straße</IonLabel>
        <IonInput
          value={address.street || ''}
          onIonChange={({ detail: { value } }) =>
            setAddress({
              ...address,
              street: value || '',
            })
          }
        />
      </IonItem>
      <IonItem>
        <IonLabel position="floating">Nr.</IonLabel>
        <IonInput
          value={address.houseNumber || ''}
          onIonChange={({ detail: { value } }) =>
            setAddress({
              ...address,
              houseNumber: value || '',
            })
          }
        />
      </IonItem>
    </div>
    <div className="addressForm display-flex">
      <IonItem>
        <IonLabel position="floating">PLZ</IonLabel>
        <IonInput
          value={address.postalCode || ''}
          inputmode="numeric"
          onIonChange={({ detail: { value } }) =>
            setAddress({
              ...address,
              postalCode: value || '',
            })
          }
        />
      </IonItem>
      <IonItem
        style={{
          flex: '1 1 100%',
        }}
      >
        <IonLabel position="floating">Ort</IonLabel>
        <IonInput
          value={address.city || ''}
          onIonChange={({ detail: { value } }) =>
            setAddress({
              ...address,
              city: value || '',
            })
          }
        />
      </IonItem>
    </div>
    {withPhone && (
      <>
        <div className="addressForm">
          <IonItem>
            <IonLabel position="floating">Telefon</IonLabel>
            <IonInput
              value={phone || ''}
              type="tel"
              pattern="tel"
              onIonChange={({ detail: { value } }) => setPhone(value || '')}
            />
          </IonItem>
        </div>
        <div className="form-hint arrow-top">
          Verrate uns deine Telefonnummer, sodass dich unser Helfer bei Rückfragen zu deinem Einkauf
          kontaktieren kann.
        </div>
      </>
    )}
  </div>
);

export default AddressForm;
