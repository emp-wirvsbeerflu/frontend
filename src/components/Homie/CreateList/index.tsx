import { Address, ListItem, ShoppingListType, ShoppingListWithUser } from '@helpinghands/types';
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonFooter,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonSlide,
  IonSlides,
  IonText,
  IonTitle,
  IonToolbar,
  IonTextarea,
} from '@ionic/react';
import { closeOutline, trashOutline } from 'ionicons/icons';
import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import { useHistory, useLocation, useRouteMatch } from 'react-router';
import useApiData from '../../../hooks/useApi';
import { HistoryState } from '../../../hooks/useHistoryState';
import { listTypesNames } from '../../../utils';
import apiRequest from '../../../utils/api-request';
import { useUser } from '../../Authentication/Provider';
import { useNotification } from '../../Global/Notification/Toast';
import AddressForm, { getLocation } from './AddressForm';
import CreateItem from './CreateItem';
import DatePicker from './DatePicker';

const CreateList: FC = () => {
  const {
    params: { id },
  } = useRouteMatch<{ id?: string }>();

  const user = useUser();

  const [userAddress] = useState(user.address);

  const slider = useRef<HTMLIonSlidesElement>(null);
  const { setError, setSuccess } = useNotification();

  const [slideIndex, setSlideIndex] = useState<number>(0);

  const [type, setType] = useState<ShoppingListType>(ShoppingListType.SUPERMARKET);
  const [items, setItems] = useState<ListItem[]>();

  const [dueDate, setDueDate] = useState<number>();
  const [comment, setComment] = useState<string>();

  const [address, setAddress] = useState<Address | undefined>(user.address);
  const [phone, setPhone] = useState(user.phone);

  const addressValid =
    !!address?.city && !!address?.houseNumber && address?.street && address?.postalCode;

  const history = useHistory<HistoryState>();

  const saveList = useCallback(async () => {
    try {
      if (id) {
        const { data } = await apiRequest.put<'/shoppinglist/:id'>(`/shoppinglist/${id}`, {
          items,
          type,
          dueDate,
          comment,
        });
        setSuccess('Dein Einkaufszettel wurde gespeichert.');
        history.replace('/', {
          updateList: data as ShoppingListWithUser,
        });
      } else if (items) {
        const { data } = await apiRequest.post<'/shoppinglist'>('/shoppinglist', {
          items,
          type,
          dueDate,
          comment,
        });
        await user.addOpenTasks();
        setSuccess('Dein Einkaufszettel wurde angelegt.');
        history.replace('/', {
          addLists: [data as ShoppingListWithUser],
        });
      }
    } catch (e) {
      setError();
    }
    // eslint-disable-next-line
  }, [history, id, items, type, comment, dueDate]);

  const { data } = useApiData<'/shoppinglist/:id'>(id ? `/shoppinglist/${id}` : undefined);

  useEffect(
    () =>
      data &&
      (setItems(data.items),
      setType(data.type),
      setDueDate(data.dueDate),
      setComment(data.comment)),
    [data],
  );

  return (
    <>
      <IonHeader>
        <IonToolbar color="primary">
          {user.address && ( // only if registration is complete
            <IonButtons slot="start">
              <IonBackButton defaultHref="/" />
            </IonButtons>
          )}
          <IonTitle>
            {
              [
                'Einkaufszettel hinzufügen',
                `Einkaufszettel ${listTypesNames[type]}`,
                'Notiz hinzufügen',
                'Kontaktdaten',
              ][slideIndex]
            }
          </IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent className="ion-padding">
        <IonSlides
          ref={slider}
          className="custom-slider"
          options={{
            initialSlide: id ? 1 : 0,
            allowTouchMove: false,
            autoHeight: false,
          }}
          onIonSlideDidChange={async () => {
            try {
              const index = await slider.current?.getActiveIndex();
              setSlideIndex(index || 0);
            } catch (e) {
              // ignore
            }
          }}
        >
          <IonSlide className="ion-justify-content-around">
            <div
              style={{
                display: 'flex',
                flexWrap: 'wrap',
                alignSelf: 'stretch',
                alignItems: 'start',
                justifyContent: 'stretch',
                width: '80%',
              }}
            >
              <h1>Wo soll für dich eingekauft werden?</h1>

              <IonRadioGroup
                value={type}
                onIonChange={({ detail: { value } }) => setType(value)}
                style={{
                  width: '100%',
                }}
                color="transparent"
              >
                <IonItem lines="none">
                  <IonRadio value={ShoppingListType.SUPERMARKET} slot="start" />
                  <IonLabel>{listTypesNames[ShoppingListType.SUPERMARKET]}</IonLabel>
                </IonItem>
                <IonItem lines="none">
                  <IonRadio value={ShoppingListType.DRUGSTORE} slot="start" />
                  <IonLabel>{listTypesNames[ShoppingListType.DRUGSTORE]}</IonLabel>
                </IonItem>
                <IonItem lines="none">
                  <IonRadio value={ShoppingListType.PHARMACY} slot="start" />
                  <IonLabel>{listTypesNames[ShoppingListType.PHARMACY]}</IonLabel>
                </IonItem>
                <IonItem lines="none">
                  <IonRadio value={ShoppingListType.MAIL} slot="start" />
                  <IonLabel>{listTypesNames[ShoppingListType.MAIL]}</IonLabel>
                </IonItem>
              </IonRadioGroup>
            </div>
          </IonSlide>
          <IonSlide>
            <div>
              <IonCard>
                <IonCardHeader>
                  <IonToolbar>
                    <div>
                      <IonCardTitle>{listTypesNames[type]}</IonCardTitle>
                      <IonCardSubtitle>
                        <DatePicker value={dueDate} setValue={setDueDate} />
                      </IonCardSubtitle>
                    </div>
                    <IonButtons slot="end">
                      <IonButton fill="clear" onClick={() => slider.current?.slidePrev()}>
                        <IonIcon slot="icon-only" src="/assets/icons/pencil.svg" />
                      </IonButton>
                    </IonButtons>
                  </IonToolbar>
                </IonCardHeader>
                <IonCardContent>
                  {items && (
                    <div
                      style={{
                        border: 'solid #ccc',
                        borderWidth: '1px 0',
                      }}
                    >
                      {items.map(({ count, name }, i) => (
                        <div
                          key={`${name}-${count}`}
                          style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}
                        >
                          <div>{count}x</div>
                          <div
                            className="ion-padding-horizontal"
                            style={{
                              flex: '1 1 100%',
                            }}
                          >
                            {name}
                          </div>
                          <div>
                            <IonButton
                              fill="clear"
                              onClick={() => setItems(items.filter((_, ind) => ind !== i))}
                              size="small"
                              shape="round"
                            >
                              <IonIcon icon={closeOutline} slot="icon-only" />
                            </IonButton>
                          </div>
                        </div>
                      ))}
                    </div>
                  )}
                  <div>
                    <CreateItem addItem={(item) => setItems([...(items || []), item])} />
                  </div>
                </IonCardContent>
              </IonCard>
              {user.address && ( // only if registration is complete
                <div
                  style={{
                    marginTop: '32px',
                  }}
                >
                  <IonButton
                    fill="clear"
                    onClick={async () => {
                      try {
                        if (id) {
                          await apiRequest.delete<'/shoppinglist/:id'>(`/shoppinglist/${id}`);
                          await user.removeOpenTasks();
                        }
                        history.replace('/', {
                          deleteList: id,
                        });
                      } catch (e) {
                        setError();
                      }
                    }}
                  >
                    <IonIcon icon={trashOutline} slot="start" />
                    Einkaufszettel löschen
                  </IonButton>
                </div>
              )}
            </div>
          </IonSlide>
          <IonSlide>
            <div>
              <h1>Gibt es etwas zu beachten?</h1>
              <IonText color="medium">
                Du kannst eine Notiz hinzufügen um z.B. auf Allergien hinzuweisen oder wenn du
                vorzugsweise günstigere Produkte möchtest.
              </IonText>
              <div className="addressForm ion-margin-top">
                <IonItem>
                  <IonTextarea
                    value={comment}
                    onIonChange={(e) => setComment(e.detail.value || undefined)}
                    placeholder="Notiz"
                  />
                </IonItem>
                <div className="form-hint arrow-top">
                  Du kannst das Feld leer lassen um keine Notiz hinzuzufügen.
                </div>
              </div>
            </div>
          </IonSlide>
          {(!userAddress || !addressValid) && (
            <IonSlide>
              <div>
                <h1>Wohin soll dein Einkauf gebracht werden?</h1>
                <IonText color="medium">
                  Du kannst deine Kontaktdaten jederzeit unter &quot;Mein Profil&quot; ändern.
                </IonText>

                <div className="ion-margin-top">
                  <AddressForm
                    address={(address || {}) as Address}
                    setAddress={(a) => setAddress(a as Address)}
                    phone={phone}
                    withPhone
                    setPhone={setPhone}
                  />
                </div>
              </div>
            </IonSlide>
          )}
        </IonSlides>
      </IonContent>
      <IonFooter className="ion-padding">
        {slideIndex === 0 && (
          <IonButton expand="block" disabled={!type} onClick={() => slider.current?.slideNext()}>
            Einkaufszettel befüllen
          </IonButton>
        )}
        {slideIndex === 1 && (
          <IonButton
            expand="block"
            disabled={!(items || []).length}
            onClick={() => {
              slider.current?.slideNext();
            }}
          >
            Einkaufszettel speichern
          </IonButton>
        )}
        {slideIndex === 2 && (
          <IonButton
            expand="block"
            onClick={() => {
              if (!user.address || !addressValid) {
                slider.current?.slideNext();
              } else {
                saveList();
              }
            }}
          >
            {comment?.length ? 'Notiz hinzufügen' : 'Ohne Notiz fortfahren'}
          </IonButton>
        )}
        {slideIndex === 3 && (
          <IonButton
            expand="block"
            disabled={!addressValid}
            onClick={async () => {
              try {
                if (address && items) {
                  const location = await getLocation(address);

                  await user.updateUser({
                    address,
                    phone,
                    location,
                  });

                  await saveList();
                }
              } catch (e) {
                setError('Die Adresse wurde nicht gefunden');
              }
            }}
          >
            Kontaktdaten speichern
          </IonButton>
        )}
      </IonFooter>
    </>
  );
};

// We need the key here to reset the state
export default () => {
  const { key, pathname } = useLocation();

  if (!pathname.startsWith('/list')) {
    return null;
  }

  return (
    <IonPage>
      <CreateList key={key} />
    </IonPage>
  );
};
