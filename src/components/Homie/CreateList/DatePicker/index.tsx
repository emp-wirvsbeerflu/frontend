import { IonDatetime } from '@ionic/react';
import React, { FC } from 'react';
import DateBadge from '../../../Global/DateBadge';

const dateToString = (date: Date, addYear = 0) =>
  `${date.getFullYear() + addYear}-${date
    .getMonth()
    .toString()
    .padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;

const DatePicker: FC<{
  value?: number;
  setValue: (date?: number) => void;
}> = ({ value, setValue }) => {
  return value ? (
    <DateBadge value={value} onClear={() => setValue(undefined)} />
  ) : (
    <IonDatetime
      min={dateToString(new Date())}
      max={dateToString(new Date(), 1)}
      value={value ? dateToString(new Date(value)) : undefined}
      placeholder="Zieldatum festlegen"
      doneText="OK"
      cancelText="ABBRECHEN"
      displayFormat="YYYY-MM-DD"
      onIonChange={(e) => e.detail.value && setValue(new Date(e.detail.value).valueOf())}
      style={{
        border: '1px solid currentColor',
        borderRadius: '12px',
        display: 'inline-block',
        padding: '2px 8px',
        height: '20px',
        color: '#000',
        fontSize: '12px',
      }}
    />
  );
};

export default DatePicker;
