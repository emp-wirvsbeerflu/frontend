import { ListItem } from '@helpinghands/types';
import { IonButton, IonIcon, IonInput, IonLabel } from '@ionic/react';
import { addOutline } from 'ionicons/icons';
import React, { FC, useState } from 'react';

const CreateItem: FC<{
  addItem: (item: ListItem) => void;
}> = ({ addItem }) => {
  const [count, setCount] = useState<number>(1);
  const [name, setName] = useState<string>('');

  return (
    <div className="addItem display-flex ion-margin-vertical">
      <style>
        {`
        div.addItem ion-input {
          border: 1px solid currentColor;
          border-radius: 4px;
        }
        div.addItem ion-button {
          height: 44px;
          margin: 0;
        }
        div.addItem ion-input.no-padding-left input {
          padding-left: 0;
        }
        
        div.addItem div:not(:last-of-type) {
          margin-right: 8px;
        }
      `}
      </style>
      <div
        style={{
          flex: '0 0',
          textAlign: 'center',
        }}
      >
        <IonLabel position="stacked">Anzahl</IonLabel>
        <IonInput
          value={count || ''}
          inputMode="numeric"
          className="no-padding-left"
          onIonChange={({ detail: { value } }) => setCount(parseInt(value || '', 10))}
        />
      </div>
      <div
        style={{
          flex: '1 1 100%',
        }}
      >
        <IonLabel position="stacked">Artikel</IonLabel>
        <IonInput value={name} onIonChange={({ detail: { value } }) => setName(value || '')} />
      </div>
      <div className="ion-align-self-end">
        <IonButton
          color="primary"
          disabled={!count || !name}
          onClick={() => {
            addItem({
              done: false,
              name,
              count,
            });
            setCount(1);
            setName('');
          }}
        >
          <IonIcon slot="icon-only" icon={addOutline} />
        </IonButton>
      </div>
    </div>
  );
};

export default CreateItem;
