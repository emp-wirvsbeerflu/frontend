import { ShoppingListState, ShoppingListWithUser } from '@helpinghands/types';
import {
  IonAvatar,
  IonBackButton,
  IonButtons,
  IonCardSubtitle,
  IonCardTitle,
  IonHeader,
  IonItem,
  IonLabel,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import React, { FC } from 'react';
import { listTypesNames } from '../../../../utils';
import apostroph from '../../../../utils/apostroph';

const ListHeader: FC<{
  data?: ShoppingListWithUser;
}> = ({ data }) => (
  <IonHeader>
    <IonToolbar color="primary">
      <IonButtons slot="start">
        <IonBackButton defaultHref="/" />
      </IonButtons>
      {data && <IonTitle>{apostroph(data.displayName)} Einkaufszettel</IonTitle>}
    </IonToolbar>
    {data && (
      <IonToolbar>
        <IonItem lines="none">
          {data.photoURL && (
            <IonAvatar slot="start">
              <img alt="avatar" src={data.photoURL} />
            </IonAvatar>
          )}
          <IonLabel>
            {data.state === ShoppingListState.TAKEN ? (
              <>
                <IonCardSubtitle>{apostroph(data.displayName)} Einkaufszettel</IonCardSubtitle>
                <IonCardTitle>{listTypesNames[data.type]}</IonCardTitle>
              </>
            ) : (
              <>
                <IonCardSubtitle>Bringe den Einkauf zu</IonCardSubtitle>
                <IonCardTitle>{data.displayName}</IonCardTitle>
              </>
            )}
          </IonLabel>
        </IonItem>
      </IonToolbar>
    )}
  </IonHeader>
);

export default ListHeader;
