import React, { FC } from 'react';
import { call } from 'ionicons/icons';
import { IonButton, IonIcon, IonFooter } from '@ionic/react';

const ListFooter: FC<{
  phone?: string;
  displayName?: string;
  onButtonClick: () => void;
  btnText: string;
}> = ({ phone, onButtonClick, displayName, btnText }) => (
  <IonFooter className="ion-padding">
    {phone && (
      <IonButton expand="block" fill="outline" href={`tel:${phone}`} className="ion-margin-bottom">
        <IonIcon slot="start" icon={call} />
        {displayName} etwas fragen
      </IonButton>
    )}
    <IonButton color="primary" expand="block" onClick={() => onButtonClick()}>
      {btnText}
    </IonButton>
  </IonFooter>
);

export default ListFooter;
