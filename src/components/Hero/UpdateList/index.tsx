import { ShoppingListState, ShoppingListWithUser } from '@helpinghands/types';
import { IonPage } from '@ionic/react';
import React, { FC } from 'react';
import { useLocation, useRouteMatch } from 'react-router-dom';
import OnDeliveryList from './Content/on-delivery';
import TakenList from './Content/taken';
import useApiData from '../../../hooks/useApi';
import ListHeader from './Header';

const Content: FC<{ data: ShoppingListWithUser }> = ({ data }) => {
  if (data.state === ShoppingListState.TAKEN) {
    return <TakenList data={data} />;
  }
  if (data.state === ShoppingListState.ON_DELIVERY) {
    return <OnDeliveryList data={data} />;
  }
  return <TakenList data={data} />;
};

export default () => {
  const {
    params: { id },
  } = useRouteMatch<{ id?: string }>();

  const { search } = useLocation();

  // GET List - the search param is only for triggering a new fetch on stateChange
  const { data } = useApiData<'/shoppinglist/:id'>(`/shoppinglist/${id}${search}`);

  return (
    <IonPage>
      <ListHeader data={data} />
      {data && <Content data={data} />}
    </IonPage>
  );
};
