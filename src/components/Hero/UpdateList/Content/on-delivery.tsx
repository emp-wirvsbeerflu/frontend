import { Plugins } from '@capacitor/core';
import { ShoppingListState, ShoppingListWithUser } from '@helpinghands/types';
import { IonContent } from '@ionic/react';
import React, { FC } from 'react';
import { useHistory, useRouteMatch } from 'react-router';
import { HistoryState } from '../../../../hooks/useHistoryState';
import apiRequest from '../../../../utils/api-request';
import { useUser } from '../../../Authentication/Provider';
import { useNotification } from '../../../Global/Notification/Toast';
import ListFooter from '../Footer';

const { Storage } = Plugins;

const OnDeliveryList: FC<{
  data: ShoppingListWithUser;
}> = ({ data, data: { address, phone, displayName } }) => {
  const {
    params: { id },
  } = useRouteMatch<{ id?: string }>();
  const { setError } = useNotification();
  const history = useHistory<HistoryState>();
  const user = useUser();
  return (
    <>
      <IonContent
        className="ion-padding"
        style={{
          textAlign: 'center',
        }}
      >
        <div style={{ height: '100%', display: 'flex' }} className="ion-wrap">
          <div
            className="ion-align-self-center"
            style={{
              flex: '1 1 100%',
            }}
          >
            <img alt="location" src="/assets/icons/marker.svg" />
            <h2>
              {address?.street} {address?.houseNumber}
            </h2>
            <h2>
              {address?.postalCode} {address?.city}
            </h2>
          </div>
          <div className="form-hint ion-align-self-end arrow-bottom ion-margin-bottom">
            Sobald du dein Geld von {displayName} erhalten hast, kannst du das hier bestätigen.
          </div>
        </div>
      </IonContent>
      <ListFooter
        displayName={displayName}
        btnText="Ich habe das Geld erhalten"
        phone={phone}
        onButtonClick={async () => {
          try {
            await apiRequest.post<'/shoppinglist/:id/delivered'>(
              `/shoppinglist/${id}/delivered`,
              {},
            );
            // remove key from localstorage
            await Storage.remove({
              key: `list-${id}`,
            });
            await user.removeOpenTasks();
            history.push('/', {
              updateList: {
                ...data,
                items: data.items.map((i) => ({ ...i, done: true })),
                state: ShoppingListState.DELIVERED,
              },
            });
          } catch (e) {
            setError();
          }
        }}
      />
    </>
  );
};

export default OnDeliveryList;
