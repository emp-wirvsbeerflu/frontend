import { Plugins } from '@capacitor/core';
import { ShoppingListWithUser } from '@helpinghands/types';
import {
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCheckbox,
  IonContent,
  IonItem,
  IonItemDivider,
  IonLabel,
  IonList,
  IonText,
  IonToolbar,
} from '@ionic/react';
import React, { FC, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useRouteMatch } from 'react-router-dom';
import useAlert from '../../../../hooks/useAlert';
import useDebounce from '../../../../hooks/useDebounce';
import { HistoryState } from '../../../../hooks/useHistoryState';
import { listTypesNames } from '../../../../utils';
import apiRequest from '../../../../utils/api-request';
import DateBadge from '../../../Global/DateBadge';
import { useNotification } from '../../../Global/Notification/Toast';
import Popover from '../../../Global/Popover';
import ListFooter from '../Footer';

const { Storage } = Plugins;

const UpdateItems: FC<{
  items: boolean[];
}> = ({ items }) => {
  const {
    params: { id },
  } = useRouteMatch<{ id?: string }>();

  const debouncedItems = useDebounce(items, 30000);

  useEffect(() => {
    if (debouncedItems.length) {
      apiRequest.post<'/shoppinglist/:id/update-items'>(`/shoppinglist/${id}/update-items`, {
        items: debouncedItems,
      });
    }
  }, [debouncedItems, id]);

  return null;
};

const TakenList: FC<{
  data: ShoppingListWithUser;
}> = ({ data }) => {
  const {
    params: { id },
  } = useRouteMatch<{ id?: string }>();

  const history = useHistory<HistoryState>();

  const [checkedItems, setCheckedItems] = useState<boolean[]>([]);
  const [listHasChanged, setListHasChanged] = useState(false);

  // get data local
  useEffect(() => {
    if (data) {
      Storage.get({ key: `list-${id}` })
        .then(({ value }) => {
          if (value) {
            setCheckedItems(JSON.parse(value));
            setListHasChanged(value !== JSON.stringify(data.items.map(({ done }) => done)));
            return Promise.resolve();
          }
          return Promise.reject();
        })
        .catch(() => setCheckedItems(data.items.map(({ done }) => done)));
    }
  }, [data, id]);

  // store data local
  useEffect(() => {
    if (checkedItems.length) {
      Storage.set({
        key: `list-${id}`,
        value: JSON.stringify(checkedItems),
      });
    }
  }, [checkedItems, id]);

  const checkedArticles = data?.items.filter((_, i) => checkedItems[i]).length;

  const totalArticles = data?.items.length;

  const { setError } = useNotification();

  const reasonAlert = useAlert({
    header: 'Was ist schief gelaufen?',
    message: `Hilf ${data.displayName} zu verstehen, warum du den Einkauf abbrechen musstest.`,
    inputs: [
      {
        placeholder: 'Grund',
        name: 'reason',
        type: 'textarea',
      },
    ],
    buttons: [
      {
        text: 'Weiter Einkaufen',
        role: 'cancel',
      },
      {
        text: 'Einkauf abbrechen',
        handler: async ({ reason }) => {
          try {
            await apiRequest.post<'/shoppinglist/:id/untake'>(`/shoppinglist/${id}/untake`, {
              reason,
            });
            history.replace('/', { deleteList: id });
          } catch (e) {
            setError();
          }
        },
      },
    ],
  });

  const areYouShureAlert = useAlert({
    header: 'Möchtest du den Einkauf wirklich abbrechen?',
    message:
      'Du kannst den Einkauf natürlich jederzeit abbrechen, wenn es Schwierigkeiten gibt. Wir geben den Einkaufszettel für andere Helfer wieder frei.',
    buttons: [
      {
        text: 'Weiter Einkaufen',
        role: 'cancel',
      },
      {
        text: 'Einkauf abbrechen',
        handler: () => reasonAlert.open(),
      },
    ],
  });

  const deliverAlert = useAlert({
    cssClass: 'price-alert',
    header: 'Wie viel hat der Einkauf gekostet?',
    message: `Bitte bringe ${data.displayName} den Kassenbon mit.`,
    inputs: [
      {
        name: 'price',
        type: 'number',
        placeholder: 'Betrag',
      },
    ],
    buttons: [
      {
        text: 'Abbrechen',
        role: 'cancel',
      },
      {
        text: 'Bestätigen',
        handler: async ({ price: val = '' }) => {
          try {
            const price = parseFloat(val);
            await apiRequest.post<'/shoppinglist/:id/deliver'>(`/shoppinglist/${id}/deliver`, {
              price,
              items: checkedItems,
            });
            // trigger a new fetch
            history.replace({
              ...history.location,
              search: 'deliver=true',
            });
          } catch (e) {
            setError();
          }
        },
      },
    ],
  });

  const uncompleteAlert = useAlert({
    header: 'Nicht alles bekommen?',
    message: `Du kannst ${data.displayName} anrufen, um nach alternativen Produkten zu fragen.`,
    buttons: [
      {
        text: 'Weiter einkaufen',
        role: 'cancel',
      },
      {
        text: 'Trotzdem abschließen',
        handler: () => deliverAlert.open(),
      },
    ],
  });

  return (
    <>
      {areYouShureAlert.element}
      {reasonAlert.element}
      {deliverAlert.element}
      {uncompleteAlert.element}
      <IonContent className="ion-padding">
        {listHasChanged && <UpdateItems items={checkedItems} />}
        <IonCard>
          <IonCardHeader>
            <IonToolbar>
              <IonCardTitle>{listTypesNames[data?.type]}</IonCardTitle>
              <IonCardSubtitle>
                {checkedArticles}/{totalArticles} Artikel{' '}
                {data.dueDate && <DateBadge value={data.dueDate} />}
              </IonCardSubtitle>
              <IonButtons slot="end">
                <Popover>
                  <IonList lines="none">
                    <IonItem button onClick={() => areYouShureAlert.open()}>
                      Einkauf abbrechen
                    </IonItem>
                  </IonList>
                </Popover>
              </IonButtons>
            </IonToolbar>
          </IonCardHeader>
          <IonItemDivider style={{ minHeight: '0' }} />
          <IonCardContent>
            <IonList lines="none">
              {data.items.map(({ name, count }, i) => (
                <IonItem key={name + count}>
                  <IonCheckbox
                    slot="start"
                    key={`${checkedItems[i] || false}`}
                    checked={checkedItems[i] || false}
                    onIonChange={({ detail: { checked } }) => {
                      setCheckedItems(checkedItems.map((v, index) => (index === i ? checked : v)));
                      setListHasChanged(true);
                    }}
                  />
                  <IonText slot="start">{count}x</IonText>
                  <IonLabel>{name}</IonLabel>
                </IonItem>
              ))}
            </IonList>
            {data.comment && (
              <IonText>
                Notiz:
                <br />
                {data.comment}
              </IonText>
            )}
          </IonCardContent>
        </IonCard>
      </IonContent>
      <ListFooter
        displayName={data.displayName}
        btnText="Einkauf abschließen"
        phone={data.phone}
        onButtonClick={() =>
          checkedItems.includes(false) ? uncompleteAlert.open() : deliverAlert.open()
        }
      />
    </>
  );
};

export default TakenList;
