import { Plugins } from '@capacitor/core';
import { ListItem, ShoppingListState, ShoppingListWithUser } from '@helpinghands/types';
import {
  IonAvatar,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonItem,
  IonLabel,
} from '@ionic/react';
import React, { FC, useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { listTypesNames } from '../../../../utils';
import DateBadge from '../../../Global/DateBadge';

const Task: FC<{
  task: ShoppingListWithUser;
}> = ({ task }) => {
  const [items, setItems] = useState<ListItem[]>(task.items);
  const { key } = useLocation();

  useEffect(() => {
    Plugins.Storage.get({
      key: `list-${task.id}`,
    })
      .then(({ value }) => (value ? JSON.parse(value) : Promise.reject()))
      .then((doneItems: boolean[]) =>
        setItems(
          task.items.map((v, i) => ({
            ...v,
            done: doneItems[i] || false,
          })),
        ),
      )
      .catch(() => {});
  }, [task, key]);

  const isClosed =
    task.state === ShoppingListState.CLOSED || task.state === ShoppingListState.DELIVERED;

  return (
    <IonCard className="ion-margin-top" routerLink={isClosed ? undefined : `/list/${task.id}`}>
      <IonCardHeader>
        <IonItem lines="none" color="transparent">
          {task.photoURL && (
            <IonAvatar slot="start">
              <img alt="profile" src={task.photoURL} />
            </IonAvatar>
          )}
          <IonLabel>
            <IonCardTitle>{listTypesNames[task.type]}</IonCardTitle>
            <IonCardSubtitle>
              {items.filter((item) => item.done).length}/{items.length} Artikel{' '}
              {task.dueDate && !isClosed && <DateBadge value={task.dueDate} />}
            </IonCardSubtitle>
          </IonLabel>
          {task.price && (
            <IonLabel slot="end">
              <h2>{task.price.toFixed(2)}€</h2>
              {task.state !== ShoppingListState.CLOSED && <p>Ausstehend</p>}
            </IonLabel>
          )}
        </IonItem>
      </IonCardHeader>
    </IonCard>
  );
};

export default Task;
