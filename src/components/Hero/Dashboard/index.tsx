import { ShoppingListState } from '@helpinghands/types';
import {
  IonButton,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonLabel,
  IonMenuButton,
  IonPage,
  IonSegment,
  IonSegmentButton,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import { search } from 'ionicons/icons';
import { parse } from 'querystring';
import React, { FC } from 'react';
import { useHistory } from 'react-router';
import useApiData from '../../../hooks/useApi';
import useHistoryState from '../../../hooks/useHistoryState';
import Task from './Task';
import Refresher from '../../Global/Refresh';
import Placeholder from '../../Global/Placeholder';

const Dashboard: FC = () => {
  const history = useHistory();

  const hash = parse(history.location.hash);
  const closed = hash['#closed'];

  const { data, setData, getData } = useApiData<'/hero/lists'>('/hero/lists');

  const openTasks = data?.filter(
    ({ state }) => state === ShoppingListState.TAKEN || state === ShoppingListState.ON_DELIVERY,
  );

  const closedTasks = data?.filter(
    ({ state }) => state === ShoppingListState.CLOSED || state === ShoppingListState.DELIVERED,
  );

  // Use History State to update data without new fetch
  useHistoryState({
    data,
    setData,
  });

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Meine Aufträge</IonTitle>
        </IonToolbar>
        <IonToolbar>
          <IonSegment
            onIonChange={({ detail: { value } }) =>
              history.replace(value === 'closed' ? '#closed=true' : '#')
            }
            value={closed ? 'closed' : 'active'}
          >
            <IonSegmentButton value="active">
              <IonLabel>Aktiv</IonLabel>
            </IonSegmentButton>
            <IonSegmentButton value="closed">
              <IonLabel>Abgeschlossen</IonLabel>
            </IonSegmentButton>
          </IonSegment>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <Refresher getData={getData} />
        {!closed ? (
          <>
            {openTasks?.length ? (
              <>
                {openTasks.map((task) => (
                  <Task key={task.id} task={task} />
                ))}
                <div className="form-hint arrow-top ion-align-self-end ion-margin-bottom">
                  Wähle einen Einkaufszettel, um mit dem Einkaufen loszulegen.
                </div>
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                  <IonFabButton routerLink="/homies">
                    <IonIcon icon={search} />
                  </IonFabButton>
                </IonFab>
              </>
            ) : (
              <Placeholder image="findpeople">
                <div className="form-hint">
                  Du hast keine aktiven Aufträge. Finde Menschen denen du helfen kannst!
                </div>
                <IonButton className="ion-margin" routerLink="/homies">
                  Menschen finden
                  <IonIcon icon={search} slot="end" />
                </IonButton>
              </Placeholder>
            )}
          </>
        ) : (
          <>
            {closedTasks?.length ? (
              <>
                {closedTasks.map((task) => (
                  <Task key={task.id} task={task} />
                ))}
              </>
            ) : (
              <Placeholder image="no-data">
                <div className="form-hint">
                  Du hast keine abgeschlossenen Aufträge. Finde Menschen denen du helfen kannst um
                  Aufträge abzuschließen.
                </div>
                <IonButton className="ion-margin" routerLink="/homies">
                  Menschen finden
                  <IonIcon icon={search} slot="end" />
                </IonButton>
              </Placeholder>
            )}
          </>
        )}
      </IonContent>
    </IonPage>
  );
};

export default Dashboard;
