import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonModal,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import { close } from 'ionicons/icons';
import React, { FC, useState } from 'react';
import { useUser } from '../../../Authentication/Provider';
import Location from '../../../Global/Location';
import { useNotification } from '../../../Global/Notification/Toast';

const ChangeLocation: FC<{ isOpen: boolean; setIsOpen: (value: boolean) => void }> = ({
  isOpen,
  setIsOpen,
}) => {
  const user = useUser();
  const { setError, setSuccess } = useNotification();

  const [radius, setRadius] = useState(user.radius || 15);
  const [address, setAddress] = useState(user.address);
  const [location, setLocation] = useState(user.location);

  return (
    <IonModal isOpen={isOpen} swipeToClose onDidDismiss={() => setIsOpen(false)}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Umkreis ändern</IonTitle>
          <IonButtons slot="end">
            <IonButton onClick={() => setIsOpen(false)}>
              <IonIcon slot="icon-only" icon={close} />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <Location
          location={location}
          radius={radius}
          setAddress={setAddress}
          setLocation={setLocation}
          setRadius={setRadius}
          initialAddress={address?.city}
          button={
            <IonButton
              expand="block"
              style={{
                position: 'sticky',
                bottom: '0',
                zIndex: '10',
              }}
              onClick={async () => {
                try {
                  await user.updateUser({
                    address,
                    radius,
                    location,
                  });
                  setIsOpen(false);
                  setSuccess('Die Suchkriterien wurden gespeichert.');
                } catch (e) {
                  setError();
                }
              }}
            >
              Umkreis festlegen
            </IonButton>
          }
        />
      </IonContent>
    </IonModal>
  );
};

export default ChangeLocation;
