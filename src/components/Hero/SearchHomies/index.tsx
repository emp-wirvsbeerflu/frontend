import {
  IonAvatar,
  IonButtons,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonItem,
  IonItemGroup,
  IonLabel,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
} from '@ionic/react';
import React, { useState } from 'react';
import { FormattedPlural } from 'react-intl';
import useApiData from '../../../hooks/useApi';
import { useUser } from '../../Authentication/Provider';
import Refresher from '../../Global/Refresh';
import ChangeLocation from './ChangeLocation';
import Placeholder from '../../Global/Placeholder';

export default () => {
  const user = useUser();
  const { data, getData } = useApiData<'/homies/:page?'>('/homies', user.location);

  const [locationModal, setLocationModal] = useState(false);

  const { address, radius } = user || {};

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Aufträge finden</IonTitle>
        </IonToolbar>
        <IonItemGroup>
          <IonItem lines="none">
            <IonIcon slot="start" src="/assets/icons/location-on.svg" />
            <IonLabel>
              <b>Ort:</b> {address?.city} {address?.street} {address?.houseNumber}
            </IonLabel>
          </IonItem>
          <IonItem lines="none">
            <IonIcon slot="start" src="/assets/icons/location-radius.svg" />
            <IonLabel>
              <b>Radius:</b> {radius} km
            </IonLabel>
          </IonItem>
          <IonItem lines="none">
            <IonIcon slot="start" src="/assets/icons/live-help.svg" />
            <IonLabel>
              <b>Hilfesuchende:</b> {data?.homies.length || 0}{' '}
              <FormattedPlural one="Mensch" other="Menschen" value={data?.homies.length || 0} />
            </IonLabel>
          </IonItem>
        </IonItemGroup>
        <style>{`
          ion-item-group ion-item:not(:last-child) {
            margin-bottom: -8px;
          }
       `}</style>
      </IonHeader>
      <IonContent className="ion-padding">
        <Refresher getData={getData} />
        <ChangeLocation isOpen={locationModal} setIsOpen={setLocationModal} />
        {data?.homies.length ? (
          <>
            {data?.homies.map(
              ({ photoURL, id, displayName, shoppinglistCount, pharmacy, distance }) => (
                <IonCard
                  routerLink={`/homie/${id}`}
                  key={`list-item-${id}`}
                  className="ion-margin-bottom"
                >
                  <IonCardHeader>
                    <IonToolbar color="transparent">
                      <IonCardSubtitle>
                        {distance} km | {shoppinglistCount}{' '}
                        <FormattedPlural
                          value={shoppinglistCount}
                          one="Geschäft"
                          other="Geschäfte"
                        />
                      </IonCardSubtitle>
                      <IonCardTitle>{displayName}</IonCardTitle>
                      {photoURL && (
                        <IonAvatar slot="end">
                          <img alt="avatar" src={photoURL} />
                        </IonAvatar>
                      )}
                    </IonToolbar>
                  </IonCardHeader>
                  {pharmacy && (
                    <IonItem lines="none">
                      <IonIcon
                        slot="start"
                        src="/assets/icons/pharmacy.svg"
                        style={{
                          marginRight: '8px',
                        }}
                      />
                      <IonLabel>Medikamente benötigt</IonLabel>
                    </IonItem>
                  )}
                </IonCard>
              ),
            )}

            <IonFab vertical="bottom" horizontal="end" slot="fixed">
              <IonFabButton onClick={() => setLocationModal(true)}>
                <IonIcon src="/assets/icons/location-radius-white.svg" />
              </IonFabButton>
            </IonFab>
          </>
        ) : (
          <Placeholder image="no-homies">
            <div className="form-hint">
              Wir haben niemanden in deiner Nähe gefunden, der Hilfe benötigt. Erweitere den Radius
              oder ändere den Ort um Leute zu finden, die deine Hilfe benötigen.
            </div>
            <IonButton className="ion-margin" onClick={() => setLocationModal(true)}>
              Umkreis ändern <IonIcon slot="end" src="/assets/icons/location-radius-white.svg" />
            </IonButton>
          </Placeholder>
        )}
      </IonContent>
    </IonPage>
  );
};
