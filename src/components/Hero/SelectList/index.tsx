import { ShoppingListState, ShoppingListWithUser } from '@helpinghands/types';
import {
  IonAvatar,
  IonBackButton,
  IonButton,
  IonButtons,
  IonCardContent,
  IonCardSubtitle,
  IonCardTitle,
  IonCheckbox,
  IonContent,
  IonFooter,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import React, { useState } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import useAlert from '../../../hooks/useAlert';
import useApiData from '../../../hooks/useApi';
import { HistoryState } from '../../../hooks/useHistoryState';
import { listTypesNames } from '../../../utils';
import apiRequest from '../../../utils/api-request';
import apostroph from '../../../utils/apostroph';
import { useUser } from '../../Authentication/Provider';
import CollapseCard from '../../Global/CollapsCard';
import DateBadge from '../../Global/DateBadge';
import Loading from '../../Global/Loading';
import { useNotification } from '../../Global/Notification/Toast';

export default () => {
  const {
    params: { id },
  } = useRouteMatch<{ id?: string }>();

  const [selection, setSelection] = useState<string[]>([]);

  const user = useUser();

  const { setError } = useNotification();

  const history = useHistory<HistoryState>();

  const { data } = useApiData<'/homie/:id'>(`/homie/${id}`);

  const alert = useAlert({
    message: `<b>WICHTIG: Bevor du einkaufst, solltest du kurz bei ${data?.displayName} anrufen</b> um einen Zeitpunkt für die Übergabe auszumachen.<br /><br/>Lass dir nach dem Einkauf unbedingt den <b>Kassenbon</b> ausstellen um ihn <b>${data?.displayName} vorzulegen</b>.`,
    header: `Danke, dass du ${data?.displayName} hilfst!`,
    buttons: [
      {
        text: 'Ok, verstanden!',
        handler: () =>
          data &&
          history.replace('/', {
            addLists: data.shoppinglists
              .filter((list) => selection.includes(list.id))
              .map(
                (list): ShoppingListWithUser => ({
                  ...list,
                  user: data.displayName,
                  photoURL: data.photoURL,
                  displayName: data.displayName,
                  state: ShoppingListState.TAKEN,
                }),
              ),
          }),
      },
    ],
  });

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          {data ? (
            <IonTitle>{apostroph(data.displayName)} Einkaufszettel</IonTitle>
          ) : (
            <IonTitle>Einkaufszettel</IonTitle>
          )}
        </IonToolbar>
        {data && (
          <IonToolbar>
            <IonButtons slot="start">
              <IonBackButton defaultHref="/" />
            </IonButtons>
            <IonItem lines="none">
              {data?.photoURL && (
                <IonAvatar slot="start">
                  <img alt="avatar" src={data?.photoURL} />
                </IonAvatar>
              )}
              <IonLabel>
                <IonCardSubtitle>Das benötigt</IonCardSubtitle>
                <IonCardTitle>{data?.displayName}</IonCardTitle>
              </IonLabel>
            </IonItem>
          </IonToolbar>
        )}
      </IonHeader>

      <IonContent className="ion-padding">
        <Loading isLoading={!data}>
          {data?.shoppinglists?.map((list) => (
            <CollapseCard
              key={list.id}
              title={listTypesNames[list.type]}
              style={
                selection.includes(list.id)
                  ? {
                      borderLeft: '5px solid var(--ion-color-primary)',
                    }
                  : undefined
              }
              subtitle={
                <>
                  {list.items.length} Artikel {list.dueDate && <DateBadge value={list.dueDate} />}
                </>
              }
              headerStartSlot={
                <IonCheckbox
                  color="primary"
                  onIonChange={({ detail: { checked } }) =>
                    checked
                      ? setSelection([...selection, list.id])
                      : setSelection(selection.filter((v) => v !== list.id))
                  }
                />
              }
              bottomSlot={
                <>
                  {list.comment && (
                    <IonCardContent>
                      <IonText color="medium">
                        Notiz:
                        <br />
                        {list.comment}{' '}
                      </IonText>
                    </IonCardContent>
                  )}
                </>
              }
            >
              <IonList lines="none">
                {list.items.map(({ name, count }) => (
                  <IonItem key={`${name}-${count}`}>
                    <IonText slot="start">{count}x</IonText>
                    <IonLabel>{name}</IonLabel>
                  </IonItem>
                ))}
              </IonList>
            </CollapseCard>
          ))}
          {alert.element}
        </Loading>
      </IonContent>
      <IonFooter className="ion-padding">
        <IonButton
          disabled={!selection.length}
          color="primary"
          expand="block"
          onClick={async () => {
            try {
              await apiRequest.post<'/shoppinglist/take'>('/shoppinglist/take', {
                shoppinglists: selection,
              });
              await user.addOpenTasks(selection.length);
              alert.open();
            } catch (e) {
              setError();
            }
          }}
        >
          Das bringe ich mit
        </IonButton>
      </IonFooter>
    </IonPage>
  );
};
