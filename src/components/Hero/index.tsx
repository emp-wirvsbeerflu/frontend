import { IonRouterOutlet } from '@ionic/react';
import React from 'react';
import { Route } from 'react-router';
import Settings from '../Settings';
import Dashboard from './Dashboard';
import SearchHomies from './SearchHomies';
import SelectList from './SelectList';
import UpdateList from './UpdateList';
import Support from '../Support';

const HeroRouter = () => (
  <IonRouterOutlet id="main">
    <Route path="/" exact component={Dashboard} />
    <Route path="/homies" exact component={SearchHomies} />
    <Route path="/homie/:id" component={SelectList} />
    <Route path="/list/:id" component={UpdateList} />
    <Route path="/settings" component={Settings} />
    <Route path="/support" component={Support} />
  </IonRouterOutlet>
);

export default HeroRouter;
