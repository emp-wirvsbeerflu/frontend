import { User as UserData } from '@helpinghands/types';
import React, { FC } from 'react';
import useFirebaseUser from '../../../hooks/useFirebaseUser';
import useSignIn from '../../../hooks/useSignIn';
import useUserData, {
  createUserData,
  getUserData,
  updateLocalUserData,
  updateUserData,
} from '../../../hooks/useUserData';
import { useNotification } from '../../Global/Notification/Toast';
import Provider from '../Provider';
import Register from '../Register';

/**
 * Order:
 * 1. GoogleAuth Plugin
 * 2. Firebase Auth
 * 3. HelpinHands Backend request
 */

const AuthGuard: FC = ({ children }) => {
  const { setSuccess, setError } = useNotification();

  const { authToken, signIn } = useSignIn();
  const fbUser = useFirebaseUser(authToken);
  const { user, setUser } = useUserData(fbUser);

  if (!user) {
    return (
      <Register
        googleAuth={() => signIn()}
        user={fbUser}
        onSave={async (newUser: UserData) => {
          try {
            const userData = await createUserData(newUser);
            setUser(userData);
            setSuccess('Registrierung erfolgreich');
          } catch (e) {
            setError();
          }
        }}
      />
    );
  }

  return (
    <Provider
      user={{
        ...user,
        updateUser: (newUser) => updateUserData(newUser).then(setUser),
        addOpenTasks: (number = 1) =>
          getUserData()
            .then((userData) =>
              updateLocalUserData({
                ...userData,
                openTasks: user.openTasks + number,
              }),
            )
            .then(setUser),
        removeOpenTasks: () =>
          getUserData()
            .then((userData) =>
              updateLocalUserData({
                ...userData,
                openTasks: user.openTasks - 1,
              }),
            )
            .then(setUser),
        deleteUser: () => setUser(undefined),
      }}
    >
      {children}
    </Provider>
  );
};

export default AuthGuard;
