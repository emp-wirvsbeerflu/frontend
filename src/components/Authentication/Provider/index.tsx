import { User } from '@helpinghands/types';
import React, { createContext, FC, useContext } from 'react';

export type UserWithOpenTasks = User & { openTasks: number };

interface UserContext extends UserWithOpenTasks {
  openTasks: number;
  updateUser: (user: Partial<User>) => Promise<void>;
  addOpenTasks: (number?: number) => Promise<void>;
  removeOpenTasks: () => Promise<void>;
  deleteUser: () => void;
}

const UserContext = createContext<UserContext>({} as UserContext);

export const useUser = (): UserContext => useContext(UserContext);

export const UserProvider: FC<{ user: UserContext }> = ({ user, children }) => (
  <UserContext.Provider value={user}>{children}</UserContext.Provider>
);

export default UserProvider;
