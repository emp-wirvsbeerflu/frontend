import { Address } from '@helpinghands/types';
import { IonButton, IonContent, IonPage, IonText } from '@ionic/react';
import React, { FC } from 'react';
import { LatLon } from 'use-places-autocomplete';
import Location from '../../../Global/Location';

const RegisterLocation: FC<{
  onComplete: () => void;
  radius: number;
  location?: LatLon;
  setRadius: (val: number) => void;
  setAddress: (val: Address | undefined) => void;
  setLocation: (val: LatLon | undefined) => void;
}> = ({ setAddress, onComplete, location, setLocation, radius, setRadius }) => {
  return (
    <IonPage>
      <IonContent className="register ion-padding" fullscreen>
        <div>
          <div
            style={{
              minHeight: '80vh',
            }}
          >
            <h2>Danke, dass du hilfst!</h2>
            <div className="ion-margin-bottom">
              <IonText color="medium">
                In welchem Umkreis möchtest du Menschen unterstützen?
              </IonText>
            </div>

            <Location
              setAddress={setAddress}
              location={location}
              setLocation={setLocation}
              radius={radius}
              setRadius={setRadius}
              button={
                <IonButton
                  onClick={onComplete}
                  routerLink="/homies"
                  expand="block"
                  style={{
                    position: 'sticky',
                    bottom: '0',
                    zIndex: '10',
                  }}
                >
                  einkaufen gehen
                </IonButton>
              }
            />
          </div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default RegisterLocation;
