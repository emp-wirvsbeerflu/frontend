import { UserType } from '@helpinghands/types';
import { IonAvatar, IonButton, IonContent, IonPage, IonText } from '@ionic/react';
import { User } from 'firebase';
import React, { FC } from 'react';

const Welcome: FC<{
  setUserType: (userType: UserType) => void;
  user: User;
}> = ({ setUserType, user }) => (
  <IonPage>
    <IonContent className="register ion-padding" fullscreen>
      <div>
        <div
          style={{
            maxWidth: '80%',
            margin: 'auto',
          }}
        >
          <div className="ion-margin-bottom">
            <h2 className="ion-margin-bottom">Schön, dass du da bist</h2>
            {user && (
              <IonAvatar style={{ margin: 'auto' }}>
                {user.photoURL && <img alt="profile" src={user.photoURL} />}
              </IonAvatar>
            )}
            <h4>{user?.displayName}</h4>
            <IonText color="medium">{user?.email}</IonText>
          </div>
          <div className="ion-padding-vertical" />
          <div className="ion-margin-top">
            <IonButton
              expand="block"
              className="ion-margin-bottom"
              onClick={() => setUserType(UserType.HERO)}
            >
              Ich möchte helfen
            </IonButton>
            <IonButton expand="block" onClick={() => setUserType(UserType.HOMIE)}>
              Ich suche Unterstützung
            </IonButton>
          </div>
        </div>
      </div>
    </IonContent>
  </IonPage>
);

export default Welcome;
