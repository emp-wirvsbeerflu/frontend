import { IonButton, IonContent, IonIcon, IonPage, IonText } from '@ionic/react';
import { logoGoogle } from 'ionicons/icons';
import React, { FC } from 'react';

const Index: FC<{
  googleAuth: () => void;
}> = ({ googleAuth }) => {
  return (
    <IonPage>
      <IonContent className="register ion-padding light-gradient" fullscreen>
        <div>
          <div>
            <img src="/assets/welcome_logo.svg" alt="logo" />
            <p className="ion-margin-bottom">
              Wir bringen Hilfesuchende und Helfer aus der Umgebung zusammen, um Einkäufe sicher und
              unkompliziert zu erledigen.
            </p>
            <div className="ion-padding">
              <IonButton onClick={() => googleAuth()} expand="block" fill="solid" color="light">
                <IonIcon slot="start" icon={logoGoogle} />
                mit Google anmelden
              </IonButton>
            </div>
            <div className="ion-margin" style={{ opacity: '0.7' }}>
              <IonText color="medium">
                Wenn du noch kein Konto bei uns hast, werden wir beim ersten Anmelden eins für dich
                erstellen.
              </IonText>
            </div>

            <div
              className="ion-margin section"
              style={{
                opacity: '0.7',
              }}
            >
              Mit der Registrierung erklärst du dich mit unserer{' '}
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://helping-hands.app/datenschutz/"
              >
                Datenschutzerklärung
              </a>{' '}
              und den{' '}
              <a target="_blank" rel="noopener noreferrer" href="https://helping-hands.app/agb/">
                Nutzungsbedingungen
              </a>{' '}
              einverstanden.
            </div>
          </div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Index;
