import { IonContent, IonFabButton, IonIcon, IonPage, IonText, IonRouterLink } from '@ionic/react';
import React, { FC } from 'react';

const GettingStarted: FC<{
  onComplete: () => void;
}> = ({ onComplete }) => {
  return (
    <IonPage>
      <IonContent className="register ion-padding" fullscreen>
        <div>
          <div>
            <div className="section">
              <h2>Super! Lass uns deinen ersten Einkaufszettel anlegen</h2>

              <IonText color="medium">Klicke dazu unten auf das Symbol.</IonText>
            </div>
            <div className="section">
              <IonFabButton
                routerLink="/list"
                color="primary"
                style={{
                  width: '8rem',
                  height: '8rem',
                  margin: '2rem auto',
                }}
                onClick={onComplete}
              >
                <IonIcon
                  src="/assets/icons/add_cart.svg"
                  style={{ width: '4rem', height: '4rem' }}
                />
              </IonFabButton>
            </div>
            <div className="section">
              Wenn du dich bereits auskennst, kannst du hier
              <IonRouterLink onClick={onComplete} routerLink="/">
                &nbsp;direkt in die App springen.
              </IonRouterLink>
            </div>
          </div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default GettingStarted;
