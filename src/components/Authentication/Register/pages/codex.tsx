import {
  IonContent,
  IonPage,
  IonSlides,
  IonSlide,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonButton,
  IonIcon,
} from '@ionic/react';
import React, { FC, useState, useRef } from 'react';
import { UserType } from '@helpinghands/types';
import { arrowForward, arrowBack } from 'ionicons/icons';

const content: {
  [U in UserType]: { image: string; text: string; title: string }[];
} = {
  HERO: [
    {
      image: 'handshake.png',
      title: 'Sei verbindlich',
      text: 'Der Homie ist auf deine Unterstützung angewiesen.',
    },
    {
      image: 'hands_folded.png',
      title: 'Sei pflichtbewusst',
      text: ' Erledige die Einkäufe nach bestem Wissen und Gewissen.',
    },
    {
      image: 'family.png',
      title: 'Trage Verantwortung',
      text: 'Achte auf eine möglichst kontaktlose Übergabe.',
    },
  ],
  HOMIE: [
    {
      image: 'spy.png',
      title: 'Sei achtsam',
      text: 'Lasse keine unbekannten Personen in deine Wohnung.',
    },
    {
      image: 'medical.png',
      title: 'Schütze dich',
      text: 'Achte auf eine möglichst kontaktlose Übergabe.',
    },
    {
      image: 'family.png',
      title: 'Baue Beziehungen auf',
      text: 'Dann können aus Unbekannten wertvolle Kontakte werden.',
    },
  ],
};

const Codex: FC<{
  userType: UserType;
  setCodexViewed: (val: boolean) => void;
}> = ({ userType, setCodexViewed }) => {
  const [slideIndex, setSlideIndex] = useState(0);
  const slider = useRef<HTMLIonSlidesElement>(null);
  return (
    <IonPage>
      <IonContent className="register ion-padding" fullscreen color="primary">
        <div>
          <div
            style={{
              maxWidth: '80%',
              margin: 'auto',
            }}
          >
            <div className="ion-margin-bottom">
              <h2 className="ion-margin-bottom">Perfekt! Drei Hinweise noch bevor es los geht</h2>
              <IonSlides
                pager
                ref={slider}
                className="ion-padding-vertical"
                onIonSlideDidChange={async () => {
                  try {
                    const index = await slider.current?.getActiveIndex();
                    setSlideIndex(index || 0);
                  } catch (e) {
                    // ignore
                  }
                }}
              >
                {content[userType].map(({ title, text, image }) => (
                  <IonSlide key={title}>
                    <IonCard className="ion-margin-vertical">
                      <IonCardContent>
                        <img src={`/assets/emojis/${image}`} alt={title} />
                        <IonCardHeader>
                          <h3>{title}</h3>
                        </IonCardHeader>
                        <p>{text}</p>
                      </IonCardContent>
                    </IonCard>
                  </IonSlide>
                ))}
              </IonSlides>
              <IonButton
                onClick={() => slider.current?.slidePrev()}
                fill="clear"
                color="light"
                disabled={slideIndex === 0}
              >
                <IonIcon slot="icon-only" icon={arrowBack} />
              </IonButton>
              <IonButton
                onClick={() => slider.current?.slideNext()}
                fill="clear"
                color="light"
                disabled={slideIndex === 2}
              >
                <IonIcon slot="icon-only" icon={arrowForward} />
              </IonButton>
              <style>{`
                ion-card {
                    width: 100%;
                    text-align: center;
                }
                ion-card h3 {
                    font-size: 24px !important;
                }
                .swiper-pagination-bullet {
                    opacity: 0.9;
                }
                `}</style>
              <IonButton
                color="primary"
                expand="block"
                disabled={slideIndex !== 2}
                onClick={() => setCodexViewed(true)}
              >
                Einverstanden
              </IonButton>
            </div>
          </div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Codex;
