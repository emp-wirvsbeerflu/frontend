import { User as UserData, UserType } from '@helpinghands/types';
import { User } from 'firebase';
import React, { FC, useState } from 'react';
import { LatLon } from 'use-places-autocomplete';
import Index from './pages';
import Codex from './pages/codex';
import GettingStarted from './pages/getting_startet';
import RegisterLocation from './pages/location';
import Welcome from './pages/welcome';

const Register: FC<{
  googleAuth: () => void;
  onSave: (value: UserData) => void;
  user: User | null;
}> = ({ googleAuth, user, onSave }) => {
  const [userType, setUserType] = useState<UserType>();

  const [codexViewed, setCodexViewed] = useState<boolean>(false);

  const [address, setAddress] = useState<UserData['address']>();
  const [location, setLocation] = useState<LatLon>();
  const [radius, setRadius] = useState<number>(15);

  if (!user) {
    return <Index googleAuth={googleAuth} />;
  }

  if (!userType) {
    return <Welcome setUserType={setUserType} user={user} />;
  }

  if (!codexViewed) {
    return <Codex userType={userType} setCodexViewed={setCodexViewed} />;
  }

  return userType === UserType.HERO ? (
    <RegisterLocation
      location={location}
      setLocation={setLocation}
      setAddress={setAddress}
      setRadius={setRadius}
      radius={radius}
      onComplete={() =>
        onSave({
          photoURL: user?.photoURL || '',
          type: userType,
          displayName: user?.displayName || '',
          location,
          address,
          radius,
        })
      }
    />
  ) : (
    <GettingStarted
      onComplete={() =>
        onSave({
          photoURL: user?.photoURL || '',
          displayName: user?.displayName || '',
          type: UserType.HOMIE,
        })
      }
    />
  );
};

export default Register;
