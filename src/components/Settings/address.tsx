import { Address } from '@helpinghands/types';
import {
  IonButton,
  IonButtons,
  IonFooter,
  IonHeader,
  IonIcon,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import { close } from 'ionicons/icons';
import React, { FC, useState } from 'react';
import { useHistory } from 'react-router';
import { useUser } from '../Authentication/Provider';
import { useNotification } from '../Global/Notification/Toast';
import AddressForm, { getLocation } from '../Homie/CreateList/AddressForm';

const AddressModal: FC = () => {
  const user = useUser();
  const [address, setAddress] = useState<Address>(user.address as Address);

  const { setError, setSuccess } = useNotification();

  const history = useHistory();

  return (
    <>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Adressdaten</IonTitle>
          <IonButtons slot="end">
            <IonButton routerDirection="none" routerLink="#">
              <IonIcon slot="icon-only" icon={close} />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <div
        className="ion-padding"
        style={{
          height: '100%',
          backgroundColor: 'var(--background-color)',
        }}
      >
        <AddressForm
          address={address}
          setAddress={(data) =>
            setAddress({
              ...(address || {}),
              ...data,
            })
          }
        />
      </div>
      <IonFooter className="ion-padding">
        <IonButton
          routerDirection="none"
          routerLink="#"
          fill="clear"
          expand="block"
          className="ion-margin-bottom"
        >
          Abbrechen
        </IonButton>
        <IonButton
          expand="block"
          onClick={async () => {
            try {
              const location = await getLocation(address);
              await user.updateUser({
                address,
                location,
              });
              setSuccess('Die Adresse wurde erfolgreich gespeichert');
              history.replace('#');
            } catch (e) {
              setError('Die Adresse wurde nicht gefunden');
            }
          }}
        >
          Adressdaten speichern
        </IonButton>
      </IonFooter>
    </>
  );
};

export default AddressModal;
