import {
  IonButton,
  IonButtons,
  IonFooter,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import { close } from 'ionicons/icons';
import React, { FC, useState } from 'react';
import { useHistory } from 'react-router';
import authInstance from '../../utils/firebase/authentication';
import { useUser } from '../Authentication/Provider';
import { useNotification } from '../Global/Notification/Toast';

const PersonalDataModal: FC = () => {
  const { setError, setSuccess } = useNotification();
  const user = useUser();
  const [displayName, setDisplayName] = useState(user.displayName);
  const [phone, setPhone] = useState(user.phone);
  const history = useHistory();
  const { updateUser } = useUser();

  return (
    <>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Persönliche Daten</IonTitle>
          <IonButtons slot="end">
            <IonButton routerDirection="none" routerLink="#">
              <IonIcon slot="icon-only" icon={close} />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <div
        className="ion-padding addressForm"
        style={{
          height: '100%',
          backgroundColor: 'var(--background-color)',
        }}
      >
        <div>
          <IonItem>
            <IonLabel position="floating">Name</IonLabel>
            <IonInput
              type="text"
              value={displayName || ''}
              onIonChange={({ detail: { value } }) => setDisplayName(value || '')}
            />
          </IonItem>
        </div>
        <div>
          <IonItem>
            <IonLabel position="floating">Telefon</IonLabel>
            <IonInput
              value={phone || ''}
              type="tel"
              pattern="tel"
              onIonChange={({ detail: { value } }) => setPhone(value || '')}
            />
          </IonItem>
        </div>
      </div>
      <IonFooter className="ion-padding">
        <IonButton
          routerDirection="none"
          routerLink="#"
          fill="clear"
          expand="block"
          className="ion-margin-bottom"
        >
          Abbrechen
        </IonButton>
        <IonButton
          expand="block"
          onClick={async () => {
            try {
              if (displayName !== user.displayName) {
                await authInstance.currentUser?.updateProfile({
                  displayName,
                });
              }
              await updateUser({
                displayName,
                phone,
              });
              setSuccess('Die Änderungen wurde erfolgreich gespeichert');
              history.replace('#');
            } catch (e) {
              setError();
            }
          }}
        >
          Persönliche Daten speichern
        </IonButton>
      </IonFooter>
    </>
  );
};

export default PersonalDataModal;
