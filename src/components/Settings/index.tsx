import { Plugins } from '@capacitor/core';
import '@codetrix-studio/capacitor-google-auth';
import { isPlatform } from '@ionic/core';
import {
  IonAlert,
  IonAvatar,
  IonButton,
  IonButtons,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonItemGroup,
  IonLabel,
  IonMenuButton,
  IonModal,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react';
import firebase from 'firebase/app';
import { parse } from 'querystring';
import React, { useRef } from 'react';
import { useHistory } from 'react-router';
import apiRequest from '../../utils/api-request';
import authInstance from '../../utils/firebase/authentication';
import { useUser } from '../Authentication/Provider';
import { useNotification } from '../Global/Notification/Toast';
import AddressModal from './address';
import PersonalDataModal from './personal';

export default () => {
  const user = useUser();
  const history = useHistory();
  const hash = parse(history.location.hash);

  const { setSuccess, setError } = useNotification();

  const content = hash['#content'];
  const leave = hash['#leave'];

  const pageRef = useRef<HTMLElement>();

  return (
    <IonPage ref={pageRef}>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Profileinstellungen</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        {user && (
          <div
            className="display-flex"
            style={{
              minHeight: '100%',
            }}
          >
            <div
              style={{
                flex: '1 1 100%',
              }}
            >
              <IonAvatar
                style={{
                  margin: '32px auto',
                  width: '92px',
                  height: '92px',
                }}
              >
                {user?.photoURL && <img src={user.photoURL} alt="avatar" />}
              </IonAvatar>
              <IonCard className="ion-margin-top">
                <IonCardHeader>
                  <div className="display-flex">
                    <IonCardTitle>Persönliche Daten</IonCardTitle>
                    <IonButtons slot="end">
                      <IonButton routerDirection="none" routerLink="#content=personal">
                        <IonIcon slot="icon-only" src="/assets/icons/pencil.svg" />
                      </IonButton>
                    </IonButtons>
                  </div>
                </IonCardHeader>
                <IonItemGroup className="ion-padding-bottom">
                  <IonItem lines="none">
                    <IonLabel>
                      <h2>
                        <b>Name</b>
                      </h2>
                      <p>{user.displayName}</p>
                    </IonLabel>
                  </IonItem>
                  <IonItem lines="none">
                    <IonLabel>
                      <h2>
                        <b>E-Mail</b>
                      </h2>
                      <p>{authInstance.currentUser?.email}</p>
                    </IonLabel>
                  </IonItem>
                  <IonItem lines="none">
                    <IonLabel>
                      <h2>
                        <b>Telefon</b>
                      </h2>
                      <p>{user.phone || 'Telefonnummer angeben'}</p>
                    </IonLabel>
                  </IonItem>
                </IonItemGroup>
              </IonCard>
              <IonCard className="ion-margin-top">
                <IonCardHeader
                  style={{
                    paddingBottom: '0',
                  }}
                >
                  <div className="display-flex">
                    <IonCardTitle>Adressdaten</IonCardTitle>
                    <IonButtons slot="end">
                      <IonButton routerDirection="none" routerLink="#content=address">
                        <IonIcon slot="icon-only" src="/assets/icons/pencil.svg" />
                      </IonButton>
                    </IonButtons>
                  </div>
                </IonCardHeader>
                <IonItemGroup className="ion-padding-bottom">
                  <IonItem lines="none">
                    <IonLabel>
                      <h2>
                        {user.address?.street} {user.address?.houseNumber}
                      </h2>
                      <h2>
                        {user.address?.postalCode} {user.address?.city}
                      </h2>
                    </IonLabel>
                  </IonItem>
                </IonItemGroup>
              </IonCard>
              <div
                className="ion-paddingr"
                style={{
                  textAlign: 'center',
                }}
              >
                <IonButton fill="clear" routerLink="#leave=true" routerDirection="none">
                  Konto löschen
                </IonButton>
              </div>
              <IonModal
                isOpen={!!content}
                swipeToClose
                presentingElement={pageRef.current}
                onDidDismiss={() => {
                  history.replace('#');
                }}
              >
                {content === 'address' && <AddressModal />}
                {content === 'personal' && <PersonalDataModal />}
              </IonModal>
              <IonAlert
                isOpen={!!leave}
                onDidDismiss={() => history.replace('#')}
                header="Konto löschen"
                subHeader="Sicher, dass du dein Konto löschen willst?"
                message="Dieser Schritt kann NICHT rückgängig gemacht werden"
                buttons={[
                  {
                    text: 'Abbrechen',
                    role: 'cancel',
                    handler: () =>
                      setSuccess('Schön, dass du es dir nochmal anders überlegt hast! :)'),
                  },
                  {
                    text: 'Konto löschen',
                    handler: async () => {
                      try {
                        await Plugins.Storage.clear();

                        // We need to reauthenticate here
                        if (isPlatform('capacitor')) {
                          const {
                            authentication: { idToken },
                          } = await Plugins.GoogleAuth.signIn();

                          await authInstance.currentUser?.reauthenticateWithCredential(
                            firebase.auth.GoogleAuthProvider.credential(idToken),
                          );
                        } else {
                          await authInstance.currentUser?.reauthenticateWithPopup(
                            new firebase.auth.GoogleAuthProvider(),
                          );
                        }

                        await apiRequest.delete<'/user'>('/user');
                        await authInstance.currentUser?.delete();

                        setSuccess('Schade, dass du uns verlässt.. :(\nDein Konto wurde gelöscht.');
                        user.deleteUser();
                      } catch (e) {
                        setError(
                          'Irgendwas ist schiefgelaufen..\nProbiere es bitte nochmal, oder kontaktiere uns',
                        );
                      }
                    },
                  },
                ]}
              />
            </div>
          </div>
        )}
      </IonContent>
    </IonPage>
  );
};
