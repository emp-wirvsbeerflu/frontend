import { ShoppingListType } from '@helpinghands/types';

export const listTypesNames = {
  [ShoppingListType.SUPERMARKET]: 'Supermarkt',
  [ShoppingListType.PHARMACY]: 'Apotheke',
  [ShoppingListType.MAIL]: 'Post',
  [ShoppingListType.DRUGSTORE]: 'Drogerie',
};

export default listTypesNames;
