/* eslint-disable no-console */
import 'firebase/messaging';
import firebase from '.';

export default (update: (token: string) => void) => {
  const messaging = firebase.messaging();

  messaging
    .getToken()
    .then((currentToken) => currentToken && update(currentToken))
    .catch(() => {});

  return messaging.onTokenRefresh(() => {
    messaging
      .getToken()
      .then((currentToken) => currentToken && update(currentToken))
      .catch(() => {});
  });
};
