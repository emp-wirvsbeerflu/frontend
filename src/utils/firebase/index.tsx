import firebase from 'firebase/app';

firebase.initializeApp({
  apiKey: 'AIzaSyBX2wwoq0K7NN6dhLRTyMhBw3Bmile5hKw',
  authDomain: 'emp-wirvsbeerflu.firebaseapp.com',
  databaseURL: 'https://emp-wirvsbeerflu.firebaseio.com',
  projectId: 'emp-wirvsbeerflu',
  storageBucket: 'emp-wirvsbeerflu.appspot.com',
  messagingSenderId: '585174735892',
  appId: '1:585174735892:web:52e6798f96bf01f6f69b01',
  measurementId: 'G-HSD48B6XE1',
});

export default firebase;
