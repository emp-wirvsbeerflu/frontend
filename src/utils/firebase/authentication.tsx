import 'firebase/auth';
import firebase from '.';

export const { auth } = firebase;

const authInstance = auth();

export default authInstance;
