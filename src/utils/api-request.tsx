import ShoppingListApi from '@helpinghands/types';
import axios from 'axios';

/**
 * Typescript is Awesome!!
 */
type ShoppingListApiRoutesByType<T> = {
  [K in keyof ShoppingListApi]: ShoppingListApi[K] extends T ? K : never;
}[keyof ShoppingListApi];

export type APIRoutes = {
  GET: ShoppingListApiRoutesByType<{
    GET: object;
  }>;
  POST: ShoppingListApiRoutesByType<{
    POST: object;
  }>;
  PUT: ShoppingListApiRoutesByType<{
    PUT: object;
  }>;
  DELETE: ShoppingListApiRoutesByType<{
    DELETE: object;
  }>;
};

const apiRequest = {
  /**
   * Usage: `apiRequest.get<"/route">("/route")`
   */
  get: <T extends APIRoutes['GET']>(url: T | string) =>
    axios.get<ShoppingListApi[T]['GET']['return']>(url),

  /**
   * Usage: `apiRequest.post<"/route">("/route", data)`
   */
  post: <T extends APIRoutes['POST']>(
    url: T | string,
    payload: ShoppingListApi[T]['POST']['params'],
  ) => axios.post<ShoppingListApi[T]['POST']['return']>(url, payload),

  /**
   * Usage: `apiRequest.put<"/route">("/route", data)`
   */
  put: <T extends APIRoutes['PUT']>(
    url: T | string,
    payload: ShoppingListApi[T]['PUT']['params'],
  ) => axios.put<ShoppingListApi[T]['PUT']['return']>(url, payload),

  /**
   * Usage: `apiRequest.delete<"/route">("/route")`
   */
  delete: <T extends APIRoutes['DELETE']>(url: T | string) =>
    axios.delete<ShoppingListApi[T]['DELETE']>(url),
};

export default apiRequest;
