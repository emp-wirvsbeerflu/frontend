/**
 * Genitiv Apostroph
 * This is a german extra wurst to only show apostroph in specific cases:
 *
 * The genitive in German only gets an apostrophe if the name already ends in -s or an s-sound (-ss, -ß, -x, -z, -tz).
 */
export default (value: string) =>
  ['s', 'ß', 'x', 'z'].filter((v) => value.endsWith(v)).length ? `${value}’` : `${value}s`;
