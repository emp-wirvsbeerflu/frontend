export default {
  baseUrl: 'https://staging-shoppinglist-dot-emp-wirvsbeerflu.appspot.com/',
  useServiceWorker: false,
  isProduction: false,
};
