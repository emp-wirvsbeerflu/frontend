export default {
  baseUrl: 'https://shoppinglist-dot-emp-wirvsbeerflu.appspot.com/',
  useServiceWorker: true,
  isProduction: true,
};
